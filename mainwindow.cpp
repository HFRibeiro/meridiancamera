#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "stdio.h"
#include "cordef.h"
#include "GenApi/GenApi.h" //!< GenApi lib definitions.
#include "gevapi.h" //!< GEV lib definitions.
#include "FileUtil.h"
#include <sched.h>
#include <pthread.h>
#include <unistd.h>

#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>

using namespace cv;
using namespace std;

#define SAVE_VIDEO false

//############################################################//

#define MAX_NETIF 8
#define MAX_CAMERAS_PER_NETIF 32
#define MAX_CAMERAS (MAX_NETIF * MAX_CAMERAS_PER_NETIF)

// Enable/disable Bayer to RGB conversion
// (If disabled - Bayer format will be treated as Monochrome).
#define ENABLE_BAYER_CONVERSION 1

#define NUM_BUF 8
void* m_latestBuffer = nullptr;

typedef struct tagMY_CONTEXT {
    GEV_CAMERA_HANDLE camHandle;
    int depth;
    int format;
    int height;
    int width;
    UINT64 size;
    void* convertBuffer;
    BOOL convertFormat;
    BOOL exit;
} MY_CONTEXT, *PMY_CONTEXT;

MY_CONTEXT context = { 0 };
PUINT8 bufAddress[NUM_BUF];

//############################################################//

void* ImageAcquireThread(void* context)
{
    MY_CONTEXT* displayContext = (MY_CONTEXT*)context;

    int count_images = 0;
    qDebug("Start ImageAcquireThread\n");

    if (displayContext != nullptr)
	{
        // While we are still running.
        while (!displayContext->exit) 
		{
            GEV_BUFFER_OBJECT* img = nullptr;
            GEV_STATUS status = 0;

            // Wait for images to be received
            status = GevWaitForNextImage(displayContext->camHandle, &img,1000);

            if ((img != nullptr) && (status == GEVLIB_OK))
			{
                if (img->status == 0) 
				{
                    m_latestBuffer = img->address;
                    count_images++;
                    //qDebug("count_images: %d\n",count_images);
                }
                else 
				{
                    qDebug("Error getting image\n");
                    // Image had an error (incomplete (timeout/overflow/lost)).
                    // Do any handling of this condition necessary.
                }
				
            }
        }
    }
    qDebug("Exit ImageDisplayThread\n");
    pthread_exit(nullptr);
}

void* ImageDisplayThread(void* context)
{
     qDebug("Start ImageDisplayThread\n");

     namedWindow("Dalsa", WINDOW_KEEPRATIO);

     MY_CONTEXT* displayContext = (MY_CONTEXT*)context;
     if (displayContext != nullptr)
     {
         // While we are still running.
         while (!displayContext->exit)
         {
             #if SAVE_VIDEO
             VideoWriter oVideoWriter ("MyVideo.avi",CV_FOURCC('M','J','P','G'), 30, Size(width,height));
             #endif

             // Make sure we have data to save.
             if (m_latestBuffer != nullptr)
             {
                 Mat img_cam(displayContext->height, displayContext->width, CV_8UC1, m_latestBuffer);
                 imshow("Dalsa", img_cam);
                 #if SAVE_VIDEO
                 Mat colorFrame;
                 cvtColor(img_cam, colorFrame, CV_GRAY2BGR);
                 oVideoWriter.write(colorFrame);
                 #endif
             }
         }
     }
     destroyWindow("Dalsa");
     #if SAVE_VIDEO
     oVideoWriter.release();
     #endif
     qDebug("Exit ImageDisplayThread\n");
     pthread_exit(nullptr);
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    GEV_DEVICE_INTERFACE pCamera[MAX_CAMERAS] = { 0 };
    GEV_STATUS status;
    int numCamera = 0;
    int camIndex = 0;
    char uniqueName[128];
    uint32_t macLow = 0; // Low 32-bits of the mac address (for file naming).
    int i;
    UINT32 height = 0;
    UINT32 width = 0;
    UINT32 format = 0;
    UINT32 maxHeight = 1600;
    UINT32 maxWidth = 2048;
    UINT32 maxDepth = 2;
    UINT64 size;
    UINT64 payload_size;
    int numBuffers = NUM_BUF;
    GEV_CAMERA_HANDLE handle = nullptr;
    {
        GEVLIB_CONFIG_OPTIONS options = { 0 };
        GevGetLibraryConfigOptions(&options);
        //options.logLevel = GEV_LOG_LEVEL_OFF;
        //options.logLevel = GEV_LOG_LEVEL_TRACE;
        options.logLevel = GEV_LOG_LEVEL_NORMAL;
        GevSetLibraryConfigOptions(&options);
        qDebug("set default\n");
    }

    status = GevGetCameraList(pCamera, MAX_CAMERAS, &numCamera);

    int ipAddress = pCamera->ipAddr;
    char ipAddr[16];
    snprintf(ipAddr, sizeof ipAddr, "%u.%u.%u.%u", (ipAddress & 0xff000000) >> 24, (ipAddress & 0x00ff0000) >> 16, (ipAddress & 0x0000ff00) >> 8, (ipAddress & 0x000000ff));

    qDebug("Camera IP Adress: \033[0;32m%s\033[0m\n", ipAddr);

    qDebug("%d camera(s) on the network\n", numCamera);

    //====================================================================
    // Open the camera.
    status = GevOpenCamera(&pCamera[camIndex], GevExclusiveMode, &handle);
    if (status == 0)
    {
        //=================================================================
        // GenICam feature access via Camera XML File enabled by "open" Get the name of XML file name back (example only - in case you need it somewhere).
        char xmlFileName[MAX_PATH] = { 0 };
        status = GevGetGenICamXML_FileName(handle, sizeof(xmlFileName), xmlFileName);
        if (status == GEVLIB_OK) {
            qDebug("XML stored as %s\n", xmlFileName);
        }
        status = GEVLIB_OK;
    }
    // Get the low part of the MAC address (use it as part of a unique file name for saving images).
    // Generate a unique base name to be used for saving image files
    // based on the last 3 octets of the MAC address.
    macLow = pCamera[camIndex].macLow;
    macLow &= 0x00FFFFFF;
    snprintf(uniqueName, sizeof(uniqueName), "img_%06x", macLow);

    // Go on to adjust some API related settings (for tuning / diagnostics / etc....).
    if (status == 0)
    {
        GEV_CAMERA_OPTIONS camOptions = { 0 };

        // Adjust the camera interface options if desired (see the manual)
        GevGetCameraInterfaceOptions(handle, &camOptions);
        //camOptions.heartbeat_timeout_ms = 60000;		// For debugging (delay camera timeout while in debugger)
        camOptions.heartbeat_timeout_ms = 5000; // Disconnect detection (5 seconds)

        // Write the adjusted interface options back.
        GevSetCameraInterfaceOptions(handle, &camOptions);

        //=====================================================================
        // Get the GenICam FeatureNodeMap object and access the camera features.
        GenApi::CNodeMapRef* Camera = static_cast<GenApi::CNodeMapRef*>(GevGetFeatureNodeMap(handle));

        if (Camera)
        {
            // Access some features using the bare GenApi interface methods
            try
            {
                //Mandatory features....
                GenApi::CIntegerPtr ptrIntNode = Camera->_GetNode("Width");
                width = ptrIntNode->GetValue();
                ptrIntNode = Camera->_GetNode("Height");
                height = ptrIntNode->GetValue();
                ptrIntNode = Camera->_GetNode("PayloadSize");
                payload_size = ptrIntNode->GetValue();
                GenApi::CEnumerationPtr ptrEnumNode = Camera->_GetNode("PixelFormat");
                format = (UINT32)ptrEnumNode->GetIntValue();
            }
            // Catch all possible exceptions from a node access.
            CATCH_GENAPI_ERROR(status);
        }

        if (status == 0)
        {
            //=================================================================
            // Set up a grab/transfer from this camera
            //
            qDebug("Camera ROI set for:\nHeight = %d\nWidth = %d\nPixelFormat (val) = 0x%08x\n", height, width, format);

            maxHeight = height;
            maxWidth = width;
            context.height = height;
            context.width = width;
            maxDepth = GetPixelSizeInBytes(format);

            // Allocate image buffers
            // (Either the image size or the payload_size, whichever is larger - allows for packed pixel formats).
            size = maxDepth * maxWidth * maxHeight;
            size = (payload_size > size) ? payload_size : size;
            for (i = 0; i < numBuffers; i++)
            {
                bufAddress[i] = (PUINT8)malloc(size);
                memset(bufAddress[i], 0, size);
            }

            // Initialize a transfer with asynchronous buffer handling.
            status = GevInitializeTransfer(handle, Asynchronous, size, numBuffers, bufAddress);

            context.camHandle = handle;
            context.exit = true;
            context.size = size;

        }
    }

    //====================================================================

    char *feature_name = "ExposureTime";
    char *value_string = "150000"; // Limit 12456302

    GevSetFeatureValueAsString(handle, feature_name, value_string);

    feature_name = "Gain";
    value_string = "50"; // Limit 12456302

    GevSetFeatureValueAsString(handle, feature_name, value_string);

}

MainWindow::~MainWindow()
{
    context.exit = true;
    destroyAllWindows();
    delete ui;
}

void MainWindow::on_bt_start_clicked()
{
    if(context.exit)
    {
        context.exit = FALSE;
        pthread_t tid,tid2;
        pthread_create(&tid, nullptr, ImageAcquireThread, &context);
        pthread_create(&tid2, nullptr, ImageDisplayThread, &context);
        //############### Start grabbing ##############################/
        for (int i = 0; i < NUM_BUF; i++)  memset(bufAddress[i], 0, context.size);
        int status = GevStartTransfer(context.camHandle, -1);
        if (status != 0) qDebug("Error starting grab - 0x%x  or %d\n", status, status);
        else qDebug("Start grabbing\n");
    }
}

void MainWindow::on_bt_stop_clicked()
{
  if(!context.exit) context.exit = true;
}
