FROM debian:buster-slim
LABEL mantainer="Hélder Ribeiro <helder.freitas.ribeiro@gmail.com>"

RUN apt-get update \
    && apt-get install -y \
    build-essential \
    iputils-ping \
    net-tools \
    cmake \
    git \
    wget \
    unzip \
    pkg-config \
    libpcap0.8 \
    libcap2 \
    ethtool \
    libx11-dev \
    libxext-dev \
    libgtk-3-dev \
    libglade2-0 \
    libglade2-dev \
    ethtool \
    libtiff* \
    ffmpeg \
    x11-xserver-utils \
    qt5-default \
    qtmultimedia5-dev

RUN ln -s /usr/lib/x86_64-linux-gnu/libtiff.so.5 /usr/lib/x86_64-linux-gnu/libtiff.so.3

WORKDIR /home

RUN mkdir opencv \
    && cd opencv \
    && wget https://codeload.github.com/opencv/opencv/zip/3.4.5 \
    && unzip 3.4.5 \
    && cd opencv-3.4.5 \
    && mkdir build \
    && cd build \
    && cmake -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_INSTALL_PREFIX=/usr/local \
    -D WITH_TBB=ON \
    -D WITH_CUDA=OFF \
    -D WITH_V4L=ON \
    -D WITH_QT=ON \
    -D WITH_OPENGL=ON \
    .. \
    && make -j $(nproc) \
    && make install \
    && /bin/bash -c 'echo "/usr/local/lib" > /etc/ld.so.conf.d/opencv.conf' \
    && ldconfig \
    && cd /home \
    && git clone https://gitlab.com/HFRibeiro/meridiancamera.git \
    && cd /home/meridiancamera/DALSADOCKER \
    && ./corinstall all install \
    && cd /home/meridiancamera \
    && qmake \
    && make
