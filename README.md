### MeridianCamera

MeridianCamera (Círculo Meridiano de Espelho) Observatório Astronómico Prof. Manuel de Barros, Vila Nova de Gaia

#### Installs:

```
sudo apt-get -y install libpcap0.8 libcap2 ethtool libx11-dev libxext-dev libgtk-3-dev libglade2-0 libglade2-dev ethtool cmake libtiff*
sudo ln -s /usr/lib/x86_64-linux-gnu/libtiff.so.5 /usr/lib/x86_64-linux-gnu/libtiff.so.3
cd ~
mkdir opencv
cd opencv
wget https://codeload.github.com/opencv/opencv/zip/3.4.14
unzip 3.4.14
cd opencv-3.4.14
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_TBB=ON -D WITH_CUDA=OFF -D WITH_V4L=ON -D WITH_QT=ON -D WITH_OPENGL=ON ..
make -j $(nproc)
sudo make install

sudo /bin/bash -c 'echo "/usr/local/lib" > /etc/ld.so.conf.d/opencv.conf'
sudo ldconfig

cd ~
git clone https://gitlab.com/HFRibeiro/meridiancamera.git
cd DALSA
./corinstall -y all install

sudo reboot

```

### docker

```
docker build . -t meridiandockercam

sudo xhost +local:docker \
&& XSOCK=/tmp/.X11-unix \
&& XAUTH=/tmp/.docker.xauth \
&& xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -

docker run -it \
--network host \
--privileged \
-e DISPLAY=$DISPLAY \
-v $XSOCK:$XSOCK \
-v $XAUTH:$XAUTH \
-e XAUTHORITY=$XAUTH \
meridiandockercam /bin/bash

docker create -ti --name dummy meridiandockercam bash
docker cp dummy:/home/meridiancamera/meridiancamera /home/code/Desktop
```
