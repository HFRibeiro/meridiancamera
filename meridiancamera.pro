#-------------------------------------------------
#
# Project created by QtCreator 2019-01-18T13:13:50
#
#-------------------------------------------------

QT       += core gui multimedia xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = meridiancamera
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11 console

SOURCES += \
        main.cpp \
        mainwindow.cpp \
        common/GevUtils.c \
        common/convertBayer.c \
        common/GevFileUtils.c 
        

HEADERS += \
        mainwindow.h \
        common/FileUtil.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

INCLUDEPATH += /usr/local/include/opencv
LIBS += -L/usr/local/lib -lopencv_core -lopencv_imgcodecs -lopencv_highgui -lopencv_imgproc -lopencv_video -lopencv_videoio

IROOT=/usr/dalsa/GigeV

INCLUDEPATH = -I. -I$(IROOT)/include -I$(IROOT)/examples/common -I/opt/genicam_v3_0/library/CPP/include -I/usr/local/include/opencv2/

LIBS += -L/usr/local/lib -lpthread -lXext -lX11  -lGevApi -lCorW32

LIBS += -L/opt/genicam_v3_0/bin/Linux64_x64 -lGCBase_gcc421_v3_0 -lGenApi_gcc421_v3_0

INCLUDEPATH += /usr/dalsa/GigeV/include
INCLUDEPATH += /usr/dalsa/GigeV/examples/common
INCLUDEPATH += /opt/genicam_v3_0/library/CPP/include

RESOURCES += \
    rs.qrc
