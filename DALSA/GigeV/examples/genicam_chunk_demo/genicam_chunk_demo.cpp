
#include "stdio.h"
#include "cordef.h"
#include "GenApi/GenApi.h"				//!< GenApi lib definitions.
#include "GenApi/ChunkAdapterGEV.h"	//!< GenApi lib definitions.
#include "gevapi.h"						//!< GEV lib definitions.
#include "SapX11Util.h"
#include "X_Display_utils.h"
#include "FileUtil.h"
#include <sched.h>

//using namespace std;
//using namespace GenICam;
//using namespace GenApi;

#define MAX_NETIF					8
#define MAX_CAMERAS_PER_NETIF	32
#define MAX_CAMERAS		(MAX_NETIF * MAX_CAMERAS_PER_NETIF)


// Set upper limit on chunk data size in case of problems with device implementation
// (Adjust this if needed).
#define MAX_CHUNK_BYTES	256

typedef struct _GIGEV_CHUNKITEMS
{
	char *name;		// Name for chunk item.
	int	type; 	// Type of chunk item.
	
} GIGEV_CHUNKITEMS, *PGIGEV_CHUNKITEMS;

// Global chunk item list.
GIGEV_CHUNKITEMS *g_chunk_item_list = NULL;

GenApi:: CChunkAdapterGEV ChunkAdapter;

// Enable/disable Bayer to RGB conversion
// (If disabled - Bayer format will be treated as Monochrome).
#define ENABLE_BAYER_CONVERSION 1

// Enable/disable buffer FULL/EMPTY handling (cycling)
#define USE_SYNCHRONOUS_BUFFER_CYCLING	0

// Enable/disable transfer tuning (buffering, timeouts, thread affinity).
#define TUNE_STREAMING_THREADS 0

#define NUM_BUF	8
void *m_latestBuffer = NULL;

typedef struct tagMY_CONTEXT
{
   X_VIEW_HANDLE     View;
	GEV_CAMERA_HANDLE camHandle;
	int					depth;
	int 					format;
	void 					*convertBuffer;
	BOOL					convertFormat;
	BOOL              exit;
}MY_CONTEXT, *PMY_CONTEXT;

static void _GetUniqueFilename( char *filename, size_t size, char *basename)
{
	// Create a filename based on the current time (to 0.01 seconds)
	struct timeval tm;
	uint32_t years, days, hours, seconds;

	if ((filename != NULL) && (basename != NULL) )
	{
		if (size > (16 + sizeof(basename)) )
		{
	
			// Get the time and turn it into a 10 msec resolution counter to use as an index.
			gettimeofday( &tm, NULL);
			years = ((tm.tv_sec / 86400) / 365);
			tm.tv_sec = tm.tv_sec - (years*86400*365);
			days  = (tm.tv_sec / 86400);
			tm.tv_sec = tm.tv_sec - (days * 86400);
			hours = (tm.tv_sec / 3600);
			seconds = tm.tv_sec - (hours * 3600);						
															
			snprintf(filename, size, "%s_%03d%02d%04d%02d", basename, days,hours, (int)seconds, (int)(tm.tv_usec/10000));
		}
	}
}


char GetKey()
{
   char key = getchar();
   while ((key == '\r') || (key == '\n'))
   {
      key = getchar();
   }
   return key;
}

void PrintMenu()
{
   printf("GRAB CTL : [S]=stop, [1-9]=snap N, [G]=continuous, [A]=Abort\n");
   printf("MISC     : [Q]or[ESC]=end,         [T]=Toggle TurboMode (if available), [@]=SaveToFile\n");
}

void * ImageDisplayThread( void *context)
{
	MY_CONTEXT *displayContext = (MY_CONTEXT *)context;


	if (displayContext != NULL)
	{
		// While we are still running.
		while(!displayContext->exit)
		{
			GEV_BUFFER_OBJECT *img = NULL;
			GEV_STATUS status = 0;
			int chunks_attached = FALSE;
	
			// Wait for images to be received
			status = GevWaitForNextImage(displayContext->camHandle, &img, 1000);

			if ((img != NULL) && (status == GEVLIB_OK))
			{
				printf("  Frame %llu : status = %d ", (unsigned long long)img->id, img->status);
				
				if (img->status == 0)
				{
					m_latestBuffer = img->address;
					// Handle the chunk decoding (meta-data).
					{
						int i = 0;
						
						if (!chunks_attached)
						{
							GenApi::AttachStatistics_t ChunkStats;
						
							if ( ChunkAdapter.CheckBufferLayout( img->address, img->recv_size ) )
							{
								ChunkAdapter.AttachBuffer( img->address, img->recv_size, &ChunkStats );
								if ( (ChunkStats.NumAttachedChunks != 0) && (ChunkStats.NumChunkPorts != 0))
								{
									chunks_attached = TRUE;
									printf("\n  %d Total Chunks : %d chunks attached to  %d ports in ChunkAdapter", ChunkStats.NumChunks, ChunkStats.NumAttachedChunks,ChunkStats.NumChunkPorts);
								}
							}
							else
							{
								chunks_attached = FALSE;
								printf("\n  No chunks in adapter");
							}
						}
						else
						{
							// Already attached the chunk adapter to the buffer - update the buffer
							ChunkAdapter.UpdateBuffer( img->address );
						}
						printf("\n");
						
						if (chunks_attached)
						{
							// Assume all chunk item feature data will fit in a smallish string. 
							char chunk_string[128] = {0};
							int type;
							
							// Output the chunk information (crude string only output).
							i = 0;
							while (g_chunk_item_list[i].name != NULL)
							{
								GevGetFeatureValueAsString( displayContext->camHandle, g_chunk_item_list[i].name, &type, sizeof(chunk_string), chunk_string);
								printf("    Item : %s = %s\n", g_chunk_item_list[i].name, chunk_string);
								i++;
							}
							
						}
					}
					
					// Can the acquired buffer be displayed?
					if ( IsGevPixelTypeX11Displayable(img->format) || displayContext->convertFormat )
					{
						// Convert the image format if required.
						if (displayContext->convertFormat)
						{
							int gev_depth = GevGetPixelDepthInBits(img->format);
							// Convert the image to a displayable format.
							//(Note : Not all formats can be displayed properly at this time (planar, YUV*, 10/12 bit packed).
							ConvertGevImageToX11Format( img->w, img->h, gev_depth, img->format, img->address, \
													displayContext->depth, displayContext->format, displayContext->convertBuffer);
					
							// Display the image in the (supported) converted format. 
							Display_Image( displayContext->View, displayContext->depth, img->w, img->h, displayContext->convertBuffer );				
						}
						else
						{
							// Display the image in the (supported) received format. 
							Display_Image( displayContext->View, img->d,  img->w, img->h, img->address );
						}
					}
					else
					{
						//printf("Not displayable\n");
					}
				}
				else
				{
					// Image had an error (incomplete (timeout/overflow/lost)).
					// Do any handling of this condition necessary.
				}
			}
#if USE_SYNCHRONOUS_BUFFER_CYCLING
			if (img != NULL)
			{
				// Release the buffer back to the image transfer process.
				GevReleaseImage( displayContext->camHandle, img);
			}
#endif
		}
	}
	pthread_exit(0);	
}

//
// Enable Metadata (Chunk mode)
// Use the SFNC features 
//	"ChunkModeActive" (IBoolean)     -> activates inclusion of chunk data in the payload of the image.
// "ChunkSelector"   (IEnumeration) -> selects which chunk data item to control
// "ChunkEnable"     (IBoolean)     -> enables the inclusion of the selected chunk data item in the chunk section of the payload.
//
const char *intfTypeStrings[] = 
{
	"IValue", "IBase", "IInteger", "IBoolean", "ICommand", "IFloat", "IString",
	"IRegister", "ICategory", "IEnumeration", "IEnumEntry", "IPort"
};

#define CHUNK_PARSER_NAME "ChunkDataControl"

void ChunkItemCleanup( int numItems, PGIGEV_CHUNKITEMS *items )
{
	int i;
	if ( items != NULL)
	{
		GIGEV_CHUNKITEMS *tmpItems = *items;
		for (i = 0; i < numItems; i++)
		{
			if ( (tmpItems[i].name != NULL) )
			{
				free(tmpItems[i].name);
			}
		}
		free(*items);
	}
}

int ChunkModeSetup( GEV_CAMERA_HANDLE handle, int *numItem, PGIGEV_CHUNKITEMS *items )
{
	int status = 0;
	int count = 0;
	GIGEV_CHUNKITEMS *tmpItems = NULL;

	// Get the node map.
	GenApi::CNodeMapRef *Camera = static_cast<GenApi::CNodeMapRef*>(GevGetFeatureNodeMap(handle));
	if (Camera)
	{
		// Access some features using the bare GenApi interface methods
		try 
		{
			// Enable the chunk mode (if present)
			GenApi::CBooleanPtr ptrActivateNode = Camera->_GetNode("ChunkModeActive");			
			ptrActivateNode->SetValue((bool)1); 
			
			// Dalsa Nano Specific feature (ignore error in case not Nano)
			{
				GevSetFeatureValueAsString( handle, "chunkCompatibilityMode", "GenAPI");
			}
			
			// Get the selected ChunkEnable control
			GenApi::CBooleanPtr ptrEnableNode = Camera->_GetNode("ChunkEnable");

			// Get the ChunkSelector (and available selectees)
			GenApi::CEnumerationPtr ptrChunkSelector = Camera->_GetNode("ChunkSelector");
			GenApi::NodeList_t selection; 
			ptrChunkSelector->GetEntries( selection );
			
			// Iterate through the ChunkSelector items - enabling all of them 
			printf("    ChunkSelector has numEntries = %d\n", (int)selection.size());
			for ( int i = 0; i < (int)selection.size(); i++ )
			{
				GenApi::CEnumEntryPtr entry( selection[i] );
				if (GenApi::NI != entry->GetAccessMode())
				{
					const char *tmpName = (const char *)entry->GetSymbolic();
					printf("    Entry[%02d] = %s ", i, tmpName);
					ptrChunkSelector->FromString( tmpName);
					if (ptrEnableNode.IsValid() )
					{
						if ( ptrEnableNode->GetValue() )
						{
							count++;
							printf("is enabled \n");
						}
						else if ((GenApi::RW == ptrEnableNode->GetAccessMode()))
						{
							ptrEnableNode->SetValue((bool)1);
							count++;
							printf("is enabled \n");
						}
						else
						{
							printf("is NOT enabled \n");
						}
					}
				}
			} 
			
			// Now  access the chunk parser to get the names / types / offsets 
			// of the chunks just enabled.
			//
			// Assume that the "root" category of the ChunkParser XML tree is called
			// "ChunkDataControl".....
			//

			tmpItems = (PGIGEV_CHUNKITEMS)malloc( (count+1) * sizeof(GIGEV_CHUNKITEMS));
			if ( tmpItems != NULL)
			{
				int index = 0;
				GenApi::CCategoryPtr ptrCategory(Camera->_GetNode(CHUNK_PARSER_NAME));
				if( ptrCategory.IsValid() )
				{
					GenApi::FeatureList_t Features;
					ptrCategory->GetFeatures(Features);
					for( GenApi::FeatureList_t::iterator itFeature=Features.begin(); itFeature!=Features.end(); itFeature++ )
					{
						GenApi::CNodePtr node = (*itFeature);
						const char *name =  static_cast<const char *>(node->GetName());
						tmpItems[index].name = (char *)malloc( strlen(name)+1 );
						tmpItems[index].name[0] = '\0';
						strcpy(tmpItems[index].name, name);
						tmpItems[index].type = static_cast<int>(node->GetPrincipalInterfaceType());
						index++;
						if (index > count)
							break;
						
					}
					tmpItems[index].name = NULL;
				
				}
				else
				{
					printf(" Error locating category %s (assumed to be the root for the ChunkParser\n", CHUNK_PARSER_NAME); 
				}
			}

		}
		// Catch all possible exceptions from a node access.
		CATCH_GENAPI_ERROR(status);
		
		if (items != NULL)
		{
			*items = tmpItems;
		}
		else
		{
			ChunkItemCleanup( count, &tmpItems );
		}
	}
	if (numItem != NULL)
	{
		*numItem = count;
	}
	
	return status;
}


int IsTurboDriveAvailable(GEV_CAMERA_HANDLE handle)
{
	int type;
	UINT32 val = 0;
	
	if ( 0 == GevGetFeatureValue( handle, "transferTurboCurrentlyAbailable",  &type, sizeof(UINT32), &val) )
	{
		// Current / Standard method present - this feature indicates if TurboMode is available.
		// (Yes - it is spelled that odd way on purpose).
		return (val != 0);
	}
	else
	{
		// Legacy mode check - standard feature is not there try it manually.
		char pxlfmt_str[64] = {0};

		// Mandatory feature (always present).
		GevGetFeatureValueAsString( handle, "PixelFormat", &type, sizeof(pxlfmt_str), pxlfmt_str);

		// Set the "turbo" capability selector for this format.
		if ( 0 != GevSetFeatureValueAsString( handle, "transferTurboCapabilitySelector", pxlfmt_str) )
		{
			// Either the capability selector is not present or the pixel format is not part of the 
			// capability set.
			// Either way - TurboMode is NOT AVAILABLE.....
			return 0; 
		}
		else
		{
			// The capabilty set exists so TurboMode is AVAILABLE.
			// It is up to the camera to send TurboMode data if it can - so we let it.
			return 1;
		}
	}
	return 0;
}


int main(int argc, char* argv[])
{
	GEV_DEVICE_INTERFACE  pCamera[MAX_CAMERAS] = {0};
	GEV_STATUS status;
	int numCamera = 0;
	int camIndex = 0;
   X_VIEW_HANDLE  View = NULL;
	MY_CONTEXT context = {0};
   pthread_t  tid;
	char c;
	int done = FALSE;
	char uniqueName[128];
	uint32_t macLow = 0; // Low 32-bits of the mac address (for file naming).
	int numChunkItems = 0;
	int turboDriveAvailable = 0;

	// Boost application RT response (not too high since GEV library boosts data receive thread to max allowed)
	// SCHED_FIFO can cause many unintentional side effects.
	// SCHED_RR has fewer side effects.
	// SCHED_OTHER (normal default scheduler) is not too bad afer all.
	if (0)
	{
		//int policy = SCHED_FIFO;
		int policy = SCHED_RR;
		pthread_attr_t attrib;
		int inherit_sched = 0;
		struct sched_param param = {0};

		// Set an average RT priority (increase/decrease to tuner performance).
		param.sched_priority = (sched_get_priority_max(policy) - sched_get_priority_min(policy)) / 2;
		
		// Set scheduler policy
		pthread_setschedparam( pthread_self(), policy, &param); // Don't care if it fails since we can't do anyting about it.
		
		// Make sure all subsequent threads use the same policy.
		pthread_attr_init(&attrib);
		pthread_attr_getinheritsched( &attrib, &inherit_sched);
		if (inherit_sched != PTHREAD_INHERIT_SCHED)
		{
			inherit_sched = PTHREAD_INHERIT_SCHED;
			pthread_attr_setinheritsched(&attrib, inherit_sched);
		}
	}

	//===========================================================================
	// Greetings
	printf ("\nGigE Vision Library GenICam C++ Example Program (%s)\n", __DATE__);
	printf ("Copyright (c) 2017, DALSA.\nAll rights reserved.\n\n");

	//===================================================================================
	// Set default options for the library.
	{
		GEVLIB_CONFIG_OPTIONS options = {0};

		GevGetLibraryConfigOptions( &options);
		//options.logLevel = GEV_LOG_LEVEL_OFF;
		//options.logLevel = GEV_LOG_LEVEL_TRACE;
		options.logLevel = GEV_LOG_LEVEL_NORMAL;
		GevSetLibraryConfigOptions( &options);
	}

	//====================================================================================
	// DISCOVER Cameras
	//
	// Get all the IP addresses of attached network cards.

	status = GevGetCameraList( pCamera, MAX_CAMERAS, &numCamera);

	printf ("%d camera(s) on the network\n", numCamera);

	// Select the first camera found (unless the command line has a parameter = the camera index)
	if (numCamera != 0)
	{
		if (argc > 1)
		{
			sscanf(argv[1], "%d", &camIndex);
			if (camIndex >= (int)numCamera)
			{
				printf("Camera index out of range - only %d camera(s) are present\n", numCamera);
				camIndex = -1;
			}
		}

		if (camIndex != -1)
		{
			//====================================================================
			// Connect to Camera
			//
			//
			int i;
			int type;
			UINT32 height = 0;
			UINT32 width = 0;
			UINT32 format = 0;
			UINT64 size;
			UINT64 payload_size;
			int numBuffers = NUM_BUF;
			PUINT8 bufAddress[NUM_BUF];
			GEV_CAMERA_HANDLE handle = NULL;
			UINT32 pixFormat = 0;
			UINT32 pixDepth = 0;
			UINT32 convertedGevFormat = 0;
			
			//====================================================================
			// Open the camera.
			status = GevOpenCamera( &pCamera[camIndex], GevExclusiveMode, &handle);
			if (status == 0)
			{
				//=================================================================
				// GenICam feature access via Camera XML File enabled by "open"
				// 
				// Get the name of XML file name back (example only - in case you need it somewhere).
				//
				char xmlFileName[MAX_PATH] = {0};
				status = GevGetGenICamXML_FileName( handle, (int)sizeof(xmlFileName), xmlFileName);
				if (status == GEVLIB_OK)
				{
					printf("XML stored as %s\n", xmlFileName);
				}
				status = GEVLIB_OK;
			}
			// Get the low part of the MAC address (use it as part of a unique file name for saving images).
			// Generate a unique base name to be used for saving image files
			// based on the last 3 octets of the MAC address.
			macLow = pCamera[camIndex].macLow;
			macLow &= 0x00FFFFFF;
			snprintf(uniqueName, sizeof(uniqueName), "img_%06x", macLow); 
			
			// Go on to adjust some API related settings (for tuning / diagnostics / etc....).
			if ( status == 0 )
			{
				GEV_CAMERA_OPTIONS camOptions = {0};

				// Adjust the camera interface options if desired (see the manual)
				GevGetCameraInterfaceOptions( handle, &camOptions);
				//camOptions.heartbeat_timeout_ms = 60000;		// For debugging (delay camera timeout while in debugger)
				camOptions.heartbeat_timeout_ms = 5000;		// Disconnect detection (5 seconds)

#if TUNE_STREAMING_THREADS
				// Some tuning can be done here. (see the manual)
				camOptions.streamFrame_timeout_ms = 1001;				// Internal timeout for frame reception.
				camOptions.streamNumFramesBuffered = 4;				// Buffer frames internally.
				camOptions.streamMemoryLimitMax = 64*1024*1024;		// Adjust packet memory buffering limit.	
				camOptions.streamPktSize = 9180;							// Adjust the GVSP packet size.
				camOptions.streamPktDelay = 10;							// Add usecs between packets to pace arrival at NIC.
				
				// Assign specific CPUs to threads (affinity) - if required for better performance.
				{
					int numCpus = _GetNumCpus();
					if (numCpus > 1)
					{
						camOptions.streamThreadAffinity = numCpus-1;
						camOptions.serverThreadAffinity = numCpus-2;
					}
				}
#endif
				// Write the adjusted interface options back.
				GevSetCameraInterfaceOptions( handle, &camOptions);

				//=====================================================================
				// Get the GenICam FeatureNodeMap object and access the camera features.
				GenApi::CNodeMapRef *Camera = static_cast<GenApi::CNodeMapRef*>(GevGetFeatureNodeMap(handle));
				if (Camera)
				{
					// Enable metadata mode using the SFNC "Chunk" facilities.
					ChunkAdapter.AttachNodeMap( Camera->_Ptr);
					status = ChunkModeSetup( handle, &numChunkItems, &g_chunk_item_list );
					// Access some features using the bare GenApi interface methods
					try 
					{
						//Mandatory features....
						GenApi::CIntegerPtr ptrIntNode = Camera->_GetNode("Width");
						width = (UINT32) ptrIntNode->GetValue();
						ptrIntNode = Camera->_GetNode("Height");
						height = (UINT32) ptrIntNode->GetValue();
						ptrIntNode = Camera->_GetNode("PayloadSize");
						payload_size = (UINT64) ptrIntNode->GetValue();
						GenApi::CEnumerationPtr ptrEnumNode = Camera->_GetNode("PixelFormat") ;
						format = (UINT32)ptrEnumNode->GetIntValue();
					}
					// Catch all possible exceptions from a node access.
					// (There should be no errors since all of these are MANDATORY features.)
					CATCH_GENAPI_ERROR(status);
				}

				if (status == 0)
				{
					//=================================================================
					// Set up a grab/transfer from this camera
					//
					printf("Camera ROI set for \n\tHeight = %d\n\tWidth = %d\n\tPixelFormat (val) = 0x%08x\n", height,width,format);

					// Calculate the size of the image buffers.
					// (Adjust the number of lines in the buffer to fit the maximum expected chunk size - just in case)
					{
						int extra_lines = (MAX_CHUNK_BYTES + width - 1) / width;
						size = GetPixelSizeInBytes(format) * width * (height + extra_lines);
					}
					// Allocate image buffers
					// (Either the adjusted image size or the payload_size, whichever is larger - allows for packed pixel formats).

					printf("\n Image size = %d : Returned payload_size = %d \n", (int)size, (int)payload_size);				
			
					size = (payload_size > size) ? payload_size : size;
					for (i = 0; i < numBuffers; i++)
					{
						bufAddress[i] = (PUINT8)malloc(size);
						memset(bufAddress[i], 0, size);
					}
					
#if USE_SYNCHRONOUS_BUFFER_CYCLING
					// Initialize a transfer with synchronous buffer handling.
					status = GevInitializeTransfer( handle, SynchronousNextEmpty, size, numBuffers, bufAddress);
#else
					// Initialize a transfer with asynchronous buffer handling.
					status = GevInitializeTransfer( handle, Asynchronous, size, numBuffers, bufAddress);
#endif

					// Create an image display window.
					// This works best for monochrome and RGB. The packed color formats (with Y, U, V, etc..) require 
					// conversion as do, if desired, Bayer formats.
					// (Packed pixels are unpacked internally unless passthru mode is enabled).

					// Translate the raw pixel format to one suitable for the (limited) Linux display routines.			

					status = GetX11DisplayablePixelFormat( ENABLE_BAYER_CONVERSION, format, &convertedGevFormat, &pixFormat);

					if (format != convertedGevFormat) 
					{
						// We MAY need to convert the data on the fly to display it.
						if (GevIsPixelTypeRGB(convertedGevFormat))
						{
							// Conversion to RGB888 required.
							pixDepth = 32;	// Assume 4 8bit components for color display (RGBA)
							context.format = Convert_SaperaFormat_To_X11( pixFormat);
							context.depth = pixDepth;
							context.convertBuffer = malloc((width * height * ((pixDepth + 7)/8)));
							context.convertFormat = TRUE;
						}
						else
						{
							// Converted format is MONO - generally this is handled
							// internally (unpacking etc...) unless in passthru mode.
							// (						
							pixDepth = GevGetPixelDepthInBits(convertedGevFormat);
							context.format = Convert_SaperaFormat_To_X11( pixFormat);
							context.depth = pixDepth;							
							context.convertBuffer = NULL;
							context.convertFormat = FALSE;
						}
					}
					else
					{
						pixDepth = GevGetPixelDepthInBits(convertedGevFormat);
						context.format = Convert_SaperaFormat_To_X11( pixFormat);
						context.depth = pixDepth;
						context.convertBuffer = NULL;
						context.convertFormat = FALSE;
					}
					
					View = CreateDisplayWindow("GigE-V GenApi Console Demo", TRUE, height, width, pixDepth, pixFormat, FALSE ); 

					// Create a thread to receive images from the API and display them.
					context.View = View;
					context.camHandle = handle;
					context.exit = FALSE;
		   		pthread_create(&tid, NULL, ImageDisplayThread, &context); 

		         // Call the main command loop or the example.
		         PrintMenu();
		         while(!done)
		         {
		            c = GetKey();

		            // Toggle turboMode
		            if ((c == 'T') || (c=='t'))
		            {
							// See if TurboDrive is available.
							turboDriveAvailable = IsTurboDriveAvailable(handle);
							if (turboDriveAvailable)
							{
								UINT32 val = 1;
								GevGetFeatureValue(handle, "transferTurboMode", &type, sizeof(UINT32), &val);
								val = (val == 0) ? 1 : 0;
								GevSetFeatureValue(handle, "transferTurboMode", sizeof(UINT32), &val);
								GevGetFeatureValue(handle, "transferTurboMode", &type, sizeof(UINT32), &val);
								if (val == 1)
								{
									printf("TurboMode Enabled\n"); 	
								}
								else
								{
									printf("TurboMode Disabled\n"); 	
								}														
							}
							else
							{
								printf("*** TurboDrive is NOT Available for this device/pixel format combination ***\n");
							}
		            }
		            // Stop
		            if ((c == 'S') || (c=='s') || (c == '0'))
		            {
							GevStopTransfer(handle);
		            }
		            //Abort
		            if ((c == 'A') || (c=='a'))
		            {
	 						GevAbortTransfer(handle);
						}
		            // Snap N (1 to 9 frames)
		            if ((c >= '1')&&(c<='9'))
		            {
							for (i = 0; i < numBuffers; i++)
							{
								memset(bufAddress[i], 0, size);
							}

							status = GevStartTransfer( handle, (UINT32)(c-'0'));
							if (status != 0) printf("Error starting grab - 0x%x  or %d\n", status, status); 
						}
		            // Continuous grab.
		            if ((c == 'G') || (c=='g'))
		            {
							for (i = 0; i < numBuffers; i++)
							{
								memset(bufAddress[i], 0, size);
							}
	 						status = GevStartTransfer( handle, -1);
							if (status != 0) printf("Error starting grab - 0x%x  or %d\n", status, status); 
		            }
					
		            // Save image
		            if ((c == '@'))
		            {
							char filename[128] = {0};
							int ret = -1;
							uint32_t saveFormat = format;
							void *bufToSave = m_latestBuffer;
							int allocate_conversion_buffer = 0;
							
							// Make sure we have data to save.
							if ( m_latestBuffer != NULL )
							{
								uint32_t component_count = 1;
								UINT32 convertedFmt = 0;
								
								// Bayer conversion enabled for save image to file option.
								//
								// Get the converted pixel type received from the API that is 
								//	based on the pixel type output from the camera.
								// (Packed formats are automatically unpacked - unless in "passthru" mode.)
								//
								convertedFmt = GevGetConvertedPixelType( 0, format);
								
								if ( GevIsPixelTypeBayer( convertedFmt ) && ENABLE_BAYER_CONVERSION )
								{
									int img_size = 0;
									int img_depth = 0;
									uint8_t fill = 0;
									
									// Bayer will be converted to RGB.
									saveFormat = GevGetBayerAsRGBPixelType(convertedFmt);
									
									// Convert the image to RGB.
									img_depth = GevGetPixelDepthInBits(saveFormat);
									component_count = GevGetPixelComponentCount(saveFormat);
									img_size = width * height * component_count* ((img_depth + 7)/8);
									bufToSave = malloc(img_size);  
									fill = (component_count == 4) ? 0xFF : 0;  // Alpha if needed.
									memset( bufToSave, fill, img_size);
									allocate_conversion_buffer = 1;
									
									// Convert the Bayer to RGB	
									ConvertBayerToRGB( 0, height, width, convertedFmt, m_latestBuffer, saveFormat, bufToSave);

								}
								else
								{
									saveFormat = convertedFmt;
									allocate_conversion_buffer = 0;
								}
								
								// Generate a file name from the unique base name.
								_GetUniqueFilename(filename, (sizeof(filename)-5), uniqueName);
								
#if defined(LIBTIFF_AVAILABLE)
								// Add the file extension we want.
								strncat( filename, ".tif", sizeof(filename));
								
								// Write the file (from the latest buffer acquired).
								ret = Write_GevImage_ToTIFF( filename, width, height, saveFormat, bufToSave);								
								if (ret > 0)
								{
									printf("Image saved as : %s : %d bytes written\n", filename, ret); 
								}
								else
								{
									printf("Error %d saving image\n", ret);
								}
#else
								printf("*** Library libtiff not installed ***\n");
#endif
							}
							else
							{
								printf("No image buffer has been acquired yet !\n");
							}
							
							if (allocate_conversion_buffer)
							{
								free(bufToSave);
							}
						
		            }
		            if (c == '?')
		            {
		               PrintMenu();
		            }

		            if ((c == 0x1b) || (c == 'q') || (c == 'Q'))
		            {
							GevStopTransfer(handle);
		               done = TRUE;
							context.exit = TRUE;
		   				pthread_join( tid, NULL);      
		            }
		         }

					GevAbortTransfer(handle);
					status = GevFreeTransfer(handle);
					DestroyDisplayWindow(View);


					// Clean up the ChunkAdapter (before freeing the buffers and the nodemap)
					ChunkItemCleanup( numChunkItems, &g_chunk_item_list );
					ChunkAdapter.DetachBuffer();
					ChunkAdapter.DetachNodeMap();
					
					{
						// Turn OFF Chunk Mode.
						UINT32 val = 0;
						GevSetFeatureValue(handle, "ChunkModeActive", sizeof(UINT32), &val);					
					}

					for (i = 0; i < numBuffers; i++)
					{	
						free(bufAddress[i]);
					}
					if (context.convertBuffer != NULL)
					{
						free(context.convertBuffer);
						context.convertBuffer = NULL;
					}
				}
				GevCloseCamera(&handle);
			}
			else
			{
				printf("Error : 0x%0x : opening camera\n", status);
			}
		}
	}

	// Close down the API.
	GevApiUninitialize();

	// Close socket API
	_CloseSocketAPI ();	// must close API even on error


	//printf("Hit any key to exit\n");
	//kbhit();

	return 0;
}

