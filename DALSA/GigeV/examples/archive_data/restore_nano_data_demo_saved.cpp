//=========================================================================
//
// Demo showing restoring of raw data container from disk (*.gevbuf files).
// (Image alone, Image plus Metadata, other(future) ).
// 
//  Contents are then decoded.
//  (Fixed decoding for Genie-Nano cameras).
// 

#include "stdio.h"
#include "cordef.h"
#include "gevapi.h"						//!< GEV lib definitions.

//#include "SapX11Util.h"
//#include "X_Display_utils.h"
#include "FileUtil.h"

#include "gevbuffile.h"

// For Teledyne DALSA Genie Nano - the metadata layout is known in advance.

#define TELEDYNEDALSA_CHUNK_SIZE_DEVICEID      0x00000010
#define TELEDYNEDALSA_CHUNK_SIZE_DEVICEUSERID  0x00000010

typedef struct _TELEDYNEDALSA_GENIE_NANO_CHUNK
{
  UINT64 Available;
  UINT64 ExposureTime;
  UINT64 CyclingPresetCurrentActiveSet;
  UINT64 LineStatusAll;
  UINT64 AnalogGain;
  UINT64 DigitalGain;
  UINT16 OffsetX;
  UINT16 OffsetY;
  UINT32 CounterValueAtReset;
  UINT16 Width;
  UINT16 Height;
  UINT32 Reserved1;
  UINT64 Timestamp;
  struct 
  {
    UINT8 Horizontal : 4;
    UINT8 Vertcal : 4;
  } Binnings;
  UINT8  Reserved2[7];
  UINT64 TestImageSelector;
  char DeviceID[TELEDYNEDALSA_CHUNK_SIZE_DEVICEID];
  char DeviceUserID[TELEDYNEDALSA_CHUNK_SIZE_DEVICEUSERID];
  UINT64 PixelFormat;
  UINT64 ExposureDelay;
}  TELEDYNEDALSA_GENIE_NANO_CHUNK, *PTELEDYNEDALSA_GENIE_NANO_CHUNK;

typedef struct _TELEDYNEDALSA_CHUNK_INFO
{
	UINT32 chunkIDField;		// Network order field for chunkID
	UINT32 chunkSizeField;	// Network order field for chunk Size
} TELEDYNEDALSA_CHUNK_INFO, *PTELEDYNEDALSA_CHUNK_INFO;

typedef struct _TELEDYNDALSA_CHUNK_CONTAINER 
{
	TELEDYNEDALSA_CHUNK_INFO       imageChunkInfo;
	TELEDYNEDALSA_GENIE_NANO_CHUNK metadata;
	TELEDYNEDALSA_CHUNK_INFO       metadataChunkInfo;
} TELEDYNDALSA_CHUNK_CONTAINER, *PTELEDYNDALSA_CHUNK_CONTAINER;

// Set upper limit on chunk data size in case of problems with device implementation
// (Adjust this if needed).
#define MAX_CHUNK_BYTES	256

typedef struct _FILE_INFO
{
	char 					item[FILENAME_MAX];
	GEVBUFFILE 			*fdata;
} FILE_INFO, *PFILE_INFO;

void DALSA_GENIE_NANO_ImageChunkData(UINT32 ui32ChunkID, UINT32 ui32ChunkLength, BYTE* pbBuffer)
{
  TELEDYNEDALSA_GENIE_NANO_CHUNK MyInfo;

  if (sizeof(MyInfo) <= ui32ChunkLength)
  {
    memset(&MyInfo, 0, sizeof(MyInfo));
    memcpy(&MyInfo, pbBuffer, sizeof(MyInfo));

    printf("\tAvailableField....= %lx\n", MyInfo.Available);
    printf("\tExposureTime......= %ld\n", MyInfo.ExposureTime);
    printf("\tActiveCyclingSet..= %ld\n", MyInfo.CyclingPresetCurrentActiveSet);
    printf("\tLineStatusAll.....= %ld\n", MyInfo.LineStatusAll);
    printf("\tAnalogGain........= %ld\n", MyInfo.AnalogGain);
    printf("\tDigitalGain.......= %ld\n", MyInfo.DigitalGain);
    printf("\tOffsetX...........= %hd\n", MyInfo.OffsetX);
    printf("\tOffsetY...........= %hd\n", MyInfo.OffsetY);
    printf("\tWidth.............= %hd\n", MyInfo.Width);
    printf("\tHeight............= %hd\n", MyInfo.Height);
    printf("\tHorz Binning......= %d\n", (int)MyInfo.Binnings.Horizontal);
    printf("\tVert Binning......= %d\n", (int)MyInfo.Binnings.Vertcal);
    printf("\tTestImageSelector.= %ld\n", MyInfo.TestImageSelector);
    printf("\tCounterValAtReset.= %d\n", MyInfo.CounterValueAtReset);
    printf("\tTimestamp.........= %ld\n", MyInfo.Timestamp);
    printf("\tDeviceID..........= %s\n", MyInfo.DeviceID);
    printf("\tDeviceUserID......= %s\n", MyInfo.DeviceUserID);
    printf("\tPixelFormat.......= 0x%lx\n", MyInfo.PixelFormat);
    printf("\tExposureDelay.....= %ld\n", MyInfo.ExposureDelay);
    
  }
}

#define MANUAL_CHUNK_EXTRACTION 0

int ChunkInfoOutput( void *address, UINT64 data_size, void *chunk_address )
{
	if ( (address != NULL) && (data_size > 0) && (chunk_address != NULL) )
	{
		// Metadata appended to frame.
		// Handle the chunk decoding (meta-data).
		BYTE*   lpPointer = NULL;
		INT64   i64Offset = 0;
		UINT32  ui32ChunkLength = 0;
		UINT32  ui32ChunkID = 0;

		// Get a pointer to the END of the data.
		i64Offset = data_size;
		lpPointer = (BYTE *) ((BYTE *)address + i64Offset);

		// Make sure we have received enough data.
		if (i64Offset >= (INT64)sizeof(TELEDYNDALSA_CHUNK_CONTAINER))
		{
	#if (MANUAL_CHUNK_EXTRACTION == 0)
			// Extract the length of the chunk data 
			i64Offset -= 4;
			lpPointer -= 4;
			ui32ChunkLength = *(UINT32*)(lpPointer);
			ui32ChunkLength = ntohl(ui32ChunkLength);
							
			// Extract the ChunkID 
			i64Offset -= 4;
			lpPointer -= 4;
			ui32ChunkID = *(UINT32*)(lpPointer);
			ui32ChunkID = ntohl(ui32ChunkID);
							
			// Find start of metadata
			lpPointer -= ui32ChunkLength;   // This way
			lpPointer = (BYTE *)chunk_address + 0x8; // Or This way (the same).

	#else
			TELEDYNDALSA_CHUNK_CONTAINER *chunkContainer =  (TELEDYNDALSA_CHUNK_CONTAINER *)(chunk_address);
				
			ui32ChunkID     = ntohl(chunkContainer->metadataChunkInfo.chunkIDField);
			ui32ChunkLength = ntohl(chunkContainer->metadataChunkInfo.chunkSizeField);
			lpPointer = (BYTE *) &chunkContainer->metadata;
	#endif

			printf("    FramePtr:%p, PayloadSize:%ld, MetadataPtr:%p\n", address, data_size, lpPointer);
			printf("    ChunkID:0x%x, ChunkDataSize:0x%x\n", ui32ChunkID, ui32ChunkLength);
							
							
			DALSA_GENIE_NANO_ImageChunkData(ui32ChunkID, ui32ChunkLength, (BYTE*)lpPointer);
		}
	
	}
	return 0;
}

int _GetFileNames( const char *path, const char *match, int max_files, PFILE_INFO file_list)
{
	int num_files = 0;
	DIR *d;
	struct dirent *dir;
	
	if (path == NULL)
	{
		d = opendir("."); // Current dir.
	}
	else
	{
		d = opendir(path);
	}
	
	if (d)
	{
		if ( (max_files == 0) || (file_list == NULL) )
		{
			// Simply count the matching files (or all).
			while ((dir = readdir(d)) != NULL)
			{
				if (dir->d_type == DT_REG )
				{
					if ( match != NULL )
					{
						if (strstr( dir->d_name, match) != NULL)
						{
							num_files++;
						}
					}
					else
					{
						num_files++;
					}
				}
			}
		}
		else
		{
			// Copy the file names in list provided
			while ((dir = readdir(d)) != NULL)
			{
				if (dir->d_type == DT_REG )
				{
					if (num_files < max_files)
					{
						if ( match != NULL )
						{
							if (strstr( dir->d_name, match) != NULL)
							{ 
								strncpy( file_list[num_files].item, dir->d_name, FILENAME_MAX);
								num_files++;
							}
						}
						else
						{
							strncpy( file_list[num_files].item, dir->d_name, FILENAME_MAX);
							num_files++;
						}						
					}
				}
			}
		}
		closedir(d);
	}
	return num_files;
}

char GetKey()
{
   char key = getchar();
   while ((key == '\r') || (key == '\n'))
   {
      key = getchar();
   }
   return key;
}

int GetNumber()
{
	char input[MAX_PATH] = {0};
	char *nptr = NULL;
	int num;

	scanf("%s", input);
	num = (int) strtol(input, &nptr, 10);

	if (nptr == input )
	{
		return -2;
	}
	

	return num;
}

void PrintMenu()
{
   printf("FILE SELECT : [R]=Read gevbuf file (by Index), [L]=List available gevbuf files\n");
   printf("OPTIONS     : [@]=Enable/Disable Image Save (TIF) (Conversion to RGB if available)\n");
  // Future:  printf("            : [D]=Enable/Disable Image Display\n");
   printf("MISC        : [Q]or[ESC]=end\n");
}



int main(int argc, char* argv[])
{
	char c;
	int i;
	int done = FALSE;
	int enable_image_filesave = FALSE;
	int enable_image_display  = FALSE;
	int image_display_active  = FALSE;
	int num_gevbuf_files = 0;
	int selected_gevbuf_file = -1;
	FILE_INFO *gevbuf_files = NULL;


	//============================================================================
	// Greetings
	printf ("\nGigE Vision Library GenICam C Example Program (restore_data) (%s)\n", __DATE__);
	printf ("Copyright (c) 2018, DALSA. (No restrictions on further use)\n\n");

	// Get the number of raw data files currently in this directory.
	num_gevbuf_files = _GetFileNames( ".", ".gevbuf", 0, NULL);				
	if (num_gevbuf_files > 0)
	{
		gevbuf_files = (FILE_INFO *)malloc( num_gevbuf_files * sizeof(FILE_INFO));
		memset(gevbuf_files, 0, (num_gevbuf_files * sizeof(FILE_INFO)));
		if (gevbuf_files == NULL)
		{
			num_gevbuf_files = 0;
		}
		else
		{
				_GetFileNames(".", ".gevbuf", num_gevbuf_files, gevbuf_files);
		}

		// List (by index) the raw data files in this directory.
		printf("  ============================\n\n");
		printf("  Found %d available raw data files \"(*.gevbuf)\" :\n", num_gevbuf_files);
		for (i = 0; i < num_gevbuf_files; i++)
		{
			printf("\t %d :\t%s\n", i, gevbuf_files[i].item);
		}
		printf("\n  ============================\n\n");
		
	}
	else
	{
		printf(" *** No files found to restore *** \n\n");
	}


	// Call the main command loop or the example.
	PrintMenu();
	while(!done)
	{
		c = GetKey();

      // List gevbuf files.
      if ((c == 'L') || (c=='l'))
      {
			if (num_gevbuf_files == 0)
			{
				num_gevbuf_files = _GetFileNames( ".", ".gevbuf", 0, NULL);
				
				if (num_gevbuf_files > 0)
				{
					gevbuf_files = (FILE_INFO *)malloc( num_gevbuf_files * sizeof(FILE_INFO));
					memset(gevbuf_files, 0, (num_gevbuf_files * sizeof(FILE_INFO)));
					if (gevbuf_files == NULL)
					{
						num_gevbuf_files = 0;
					}
					else
					{
							_GetFileNames(".", ".gevbuf", num_gevbuf_files, gevbuf_files);
					}
				}
				else
				{
					printf(" *** No files found to restore *** \n");
				}
			}
			if (num_gevbuf_files > 0)
			{
				printf(" Found %d files : \n", num_gevbuf_files);
				for (i = 0; i < num_gevbuf_files; i++)
				{
					printf("\t %d :\t%s\n", i, gevbuf_files[i].item);
				}
			}
		}

		
      // Select a gevbuf data file to restore.
      if ((c == 'R') || (c=='r'))
      {
			int num;
			if (selected_gevbuf_file == -1)
			{
				printf("Current Data file is <none>\n");
			}
			else
			{
				printf("Current Data file is %s\n", gevbuf_files[selected_gevbuf_file].item);
			}
			
			do 
			{
				printf("Enter a Data file index (max=%d) (-1 for <none>) :", (num_gevbuf_files-1));
				fflush(stdout);
				num = GetNumber();
				printf(" Num = %d\n", num);
			} while( (num >= num_gevbuf_files) || (num < -1) );
			selected_gevbuf_file = num;
			
			// Parse the selected data file (for information)
			if (selected_gevbuf_file != -1)
			{
				GEVBUFFILE *gevBufFileRAW = NULL;
				GEVBUFFILE *gevBufFile = NULL;
				void *end_marker = NULL;
				void *usable_image_data = NULL;
				UINT32 usable_image_pixel_format = 0;
					
				// Check if we already cached the data for this one.
				if ( gevbuf_files[selected_gevbuf_file].fdata == NULL )
				{
					// Need to get the data for this file.
					unsigned long fsize = GEVBUFFILE_GetFileSize(gevbuf_files[selected_gevbuf_file].item);
					unsigned long numFrame = GEVBUFFILE_GetTotalFrames(gevbuf_files[selected_gevbuf_file].item);
					void *fdata = NULL;

					printf("File %s contains %ld frame(s)\n", gevbuf_files[selected_gevbuf_file].item, numFrame);

					// Allocate storage for file data.				
					fdata = malloc( fsize * sizeof(char));
					if (fdata != NULL)
					{
						// Get the data from the file.
						gevBufFileRAW = (GEVBUFFILE *)fdata;
						if ( GEVBUFFILE_RestoreFrameData( gevbuf_files[selected_gevbuf_file].item, fsize, gevBufFileRAW, &end_marker) == 0)
						{
							printf ("Error reading file : %s\n", gevbuf_files[selected_gevbuf_file].item);
							free(fdata);
						}
						else
						{
							gevbuf_files[selected_gevbuf_file].fdata = gevBufFileRAW;
						}
					}
				}
				
				// Parse the data (from the cache).
				if ( gevbuf_files[selected_gevbuf_file].fdata != NULL )
				{
					int count = 1;
					
					// Set up access to the first frame.
					gevBufFileRAW = (GEVBUFFILE *)gevbuf_files[selected_gevbuf_file].fdata;

					// Iterate through all frames in the container.
					do 
					{
						// See if the RAW data is compressed.
						if ( IsRawTurboModeData( gevBufFileRAW->payload_type ) )
						{
							// Need to decompress TurboMode data into new storage.
							unsigned long imgdatasize = gevBufFileRAW->h * gevBufFileRAW->w * GetPixelSizeInBytes(gevBufFileRAW->format);
							imgdatasize += MAX_CHUNK_BYTES; // Extra room in case of metadata.
							
							gevBufFile = (GEVBUFFILE *)malloc( imgdatasize + sizeof(GEVBUFFILE) - 1);
							if (gevBufFile != NULL)
							{
								UINT64 decoded_size = 0;
								UINT64 chunk_offset = 0;
								void *data  = (unsigned char *)&gevBufFileRAW->data[0]; 
								void *image = (unsigned char *)&gevBufFile->data[0];
							
								// Decompress the data into the new storage.
								Gev_DecodeTurboDriveFrame( gevBufFileRAW->payload_type, gevBufFileRAW->h, \
																	gevBufFileRAW->w, gevBufFileRAW->format, (UINT32)gevBufFileRAW->chunk_offset, \
																	gevBufFileRAW->recv_size, data, imgdatasize, image,  \
																	&decoded_size, &chunk_offset);
								
								// Update the new GEVBUFFILE info now that it is decompressed.
								gevBufFile->payload_type = GEV_PAYLOAD_TYPE_IMAGE; 							
								gevBufFile->x_offset = gevBufFileRAW->x_offset;	
								gevBufFile->y_offset = gevBufFileRAW->y_offset;	
								gevBufFile->h = gevBufFileRAW->h;			
								gevBufFile->w = gevBufFileRAW->w;			
								gevBufFile->format = gevBufFileRAW->format;		
								gevBufFile->id = gevBufFileRAW->id;		
								gevBufFile->timestamp = gevBufFileRAW->timestamp; 	
								gevBufFile->recv_size = decoded_size;		
								gevBufFile->chunk_offset = chunk_offset;	
								
							}
						}
						else
						{
							// No decompression required.
							gevBufFile = gevBufFileRAW;
						}
					
						// Output information from this file
						printf("\nFrame #%d :\n", count++);
						printf("  Type         = 0x%x\n", gevBufFile->payload_type);
						printf("  H            = %d\n", gevBufFile->h);
						printf("  W            = %d\n", gevBufFile->w);
						printf("  Format       = 0x%08x\n", gevBufFile->format);
						printf("  ID           = %ld\n", gevBufFile->id);
						printf("  Timestamp    = %ld\n", gevBufFile->timestamp);
						printf("  Chunk_offset = 0x%08x\n", (UINT32)gevBufFile->chunk_offset);						
							
						// See if there is metadata (chunk) to parse.
						if ( gevBufFile->chunk_offset != 0 )
						{
							unsigned char *ptr = (unsigned char *)&gevBufFile->data[0]; 
							// Genie Nano - chunk layout is hardcoded.						
							ChunkInfoOutput( ptr, gevBufFile->recv_size, &ptr[gevBufFile->chunk_offset] );						
						}
						
						//====================================================
						// See if this needs to be converted for further processing.
						// (The only supported processing so far is "unpacking" packed pixel formats)
						if (GevIsPixelTypePacked(gevBufFile->format))
						{
							// Packed pixel format - unpack it here. (Packed 2 pixels into 3 bytes)
							unsigned long packed_size = (gevBufFile->h * gevBufFile->w * 3) / 2;
							unsigned long unpacked_size = (gevBufFile->h * gevBufFile->w * 2);
							usable_image_data = malloc(unpacked_size);
							if (usable_image_data != NULL)
							{
								Gev_UnpackMonoPackedFrame( gevBufFile->format, packed_size, &gevBufFile->data[0], unpacked_size, usable_image_data);
								usable_image_pixel_format = GevGetUnpackedPixelType(gevBufFile->format);
							}
						}
						else
						{
							usable_image_pixel_format = gevBufFile->format;
							usable_image_data = (void *)&gevBufFile->data[0];
						}
						//=========================================================
						// Image data is available in a usable form here.
						// (A non-packed, uncompressed pixel format - Mono or RGB).
						// 
						// Note: Bayer to RGB is not yet added to this demo (To Do).
						

						//=========================================================
						// Save usable image to a file if filesave mode is enabled.
						// (Future : Save metadata in ".csv" format).
						if (enable_image_filesave && (usable_image_data != NULL) )
						{
							char filename[FILENAME_MAX] = {0};
							char *ptr;
							int ret = -1;
							int len = 0;
														
#if defined(LIBTIFF_AVAILABLE)
							// Output file in subdirectory "images"
							ret = mkdir( "./images", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
							if ( (ret == 0) || ((ret != 0) && (errno == EEXIST)) )
							{
								// Access directory "./images"
								strncpy(filename, "./images/", FILENAME_MAX);
								len = strlen(filename);
							}

							// Use the current file name as a base - removing the ".gevbuf" extension
							// and adding ".tif".
							strncpy(&filename[len], gevbuf_files[selected_gevbuf_file].item, (FILENAME_MAX-16));
							len = strlen(filename);
							ptr = strstr(filename, ".gevbuf");
							memset(ptr, 0, (FILENAME_MAX-len-1));
							sprintf(ptr, "_%ld.tif", gevBufFile->id);
							
							// Write the (current) image to a TIFF file (using legacy TIFF writer).
							ret = File_WriteTIFF( filename, gevBufFile->w, gevBufFile->h, GetPixelSizeInBytes(gevBufFile->format), GevIsPixelTypeRGB(gevBufFile->format), usable_image_data );
								
							if (ret == 0)
							{
								printf("Image saved as : %s\n", filename); 
							}
							else
							{
								printf("Error %d saving image\n", ret);
							}
#else
							printf("*** Library libtiff not installed ***\n");
#endif
						}
						//=====================================================
						
						
						// Display image if enabled (may need conversion)
						// (For image display of a sequence, all images must be the same size). 
						if (enable_image_display && (usable_image_data != NULL) )
						{
								// To Do ......
						}
						
						//=======================================================
						// Clean up temporary allocations.
						//
						// If we allocated memory for conversion - free it here.
						if ( GevIsPixelTypePacked(gevBufFile->format) )
						{
							if (usable_image_data != NULL)
							{
								free(usable_image_data);
								usable_image_data = NULL;
							}
						}	
											
						// If we allocated memory for TD decoding - free it here.
						// (This allows each frame to have different dimensions).
						if ( IsRawTurboModeData( gevBufFileRAW->payload_type ) )
						{
							if( gevBufFile != NULL)
							{
								free(gevBufFile);
								gevBufFile = NULL;
							}
						}
						//===========================================================
						
						//===========================================================
						// Get the next frame from the cached file data (if present)
						gevBufFileRAW = GEVBUFFILE_GetNextFrame( gevBufFileRAW, end_marker );
						
					} while (gevBufFileRAW != NULL);
				}
			}
		}
		
		
      // Enable Saving images as TIF (currently)
      if ((c == '@'))
      {
			if (enable_image_filesave)
			{
				enable_image_filesave = FALSE;
				printf("Image File Save DISABLED\n");
			}
			else
			{
				enable_image_filesave = TRUE;
				printf("Image File Save ENABLED\n");
			}
		}

      // Enable Displaying images
      if ((c == 'D') ||  (c == 'd'))
      {
			if (enable_image_display)
			{
				enable_image_display = FALSE;
				printf("Image Display DISABLED\n");
				if (image_display_active)
				{
					// Destroy the display window!!!
				}
			}
			else
			{
				enable_image_display = TRUE;
				printf("Image Display ENABLED\n");
			}
		}
		
      if (c == '?')
      {
			PrintMenu();
		}

		if ((c == 0x1b) || (c == 'q') || (c == 'Q'))
		{
			done = TRUE;
      }
	}

	// Clean all of this up
	if (num_gevbuf_files > 0) 
	{
		for (i = 0; i < num_gevbuf_files; i++)
		{
			if (gevbuf_files[i].fdata != NULL) free(gevbuf_files[i].fdata);
		}
		free(gevbuf_files);
	}

	return 0;
}

