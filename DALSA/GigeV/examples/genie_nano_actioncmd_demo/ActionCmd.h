/***************************************************************************************************
Copyright (c) 2016, Teledyne DALSA.
All rights reserved.
----------------------------------------------------------------------------------------------------

ActionCmd.h

Description:
Main header file for ActionCmd.c

***************************************************************************************************/

#ifndef __ACTIONCMD__H__
#define __ACTIONCMD__H__

// Disable deprecated function warnings with Visual Studio 2005
#if defined(_MSC_VER) && _MSC_VER >= 1400
#pragma warning(disable: 4995)
#pragma warning(disable: 4127)
#pragma warning(disable: 4996)
#endif


#define BUFLEN 548	//maximum datagram size to avoid IP fragmentation
                    //IP_MTU_NO_FRAGMENT_SIZE - IP_HEADER_SIZE - UDP_HEADER_SIZE = 576-20-8

// Macros
#define MALLOC(x) malloc((x))
#define FREE(x) free((x))



#define	GVCP_PORT (UINT16)0x0F74    //Standard GVCP port on device (destination port): 3956
#if 0
//===================================================
// GVCP Command Header
//===================================================

// Length in number of bytes
//#define GVCP_KEY_LENGTH		1
//#define GVCP_FLAG_LENGTH	1
//#define GVCP_COMMAND_LENGTH 2
//#define GVCP_LENGTH_LENGTH	2
//#define GVCP_ID_LENGTH		2

// Offset to use when filling the datagram
// (number of bytes)
//#define GVCP_KEY_OFFSET		0
//#define GVCP_FLAG_OFFSET	(GVCP_KEY_OFFSET + GVCP_KEY_LENGTH) 
//#define GVCP_COMMAND_OFFSET (GVCP_FLAG_OFFSET + GVCP_FLAG_LENGTH)
//#define GVCP_LENGTH_OFFSET	(GVCP_COMMAND_OFFSET + GVCP_COMMAND_LENGTH)  
//#define GVCP_ID_OFFSET		(GVCP_LENGTH_OFFSET + GVCP_LENGTH_LENGTH)
//#define GVCP_HEADER_SIZE	(GVCP_ID_OFFSET + GVCP_ID_LENGTH) 



//===================================================
// GVCP Ack Header
//===================================================

// Length in number of bytes
#define GVCP_STATUS_H	(UINT8) 0x00
#define GVCP_STATUS_L	(UINT8) 0x00
#define GVCP_ACK_H		(UINT8) 0x01
#define GVCP_ACK_L		(UINT8) 0x01
#define GVCP_LENGTH_H	(UINT8) 0x00
#define GVCP_LENGTH_L	(UINT8) 0x00
#define GVCP_STATUS_ERROR_H (UINT8) 0x80
#define GVCP_STATUS_ERROR_L (UINT8) 0x01



//===================================================
// GVCP Command Payload
//===================================================

// Length in number of bytes
#define DEVICE_KEY_LENGTH  4
#define GROUP_KEY_LENGTH   4
#define GROUP_MASK_LENGTH  4
#define ACTION_TIME_LENGTH 4

// Offset to use when filling the datagram
// (number of bytes)
#define DEVICE_KEY_OFFSET      GVCP_HEADER_SIZE
#define GROUP_KEY_OFFSET	  (DEVICE_KEY_OFFSET + GROUP_KEY_LENGTH)
#define GROUP_MASK_OFFSET     (GROUP_KEY_OFFSET + GROUP_KEY_LENGTH)
#define ACTION_TIME_H_OFFSET  (GROUP_MASK_OFFSET + GROUP_MASK_LENGTH)
#define ACTION_TIME_L_OFFSET  (ACTION_TIME_H_OFFSET + ACTION_TIME_LENGTH)
#endif

// Misc defs 
#define TRUE  1
#define FALSE 0
typedef uint32_t DWORD, UINT32, *PUINT32;
typedef uint16_t UINT16, *PUINT16;
typedef uint8_t  UINT8, *PUINT8;

typedef int BOOL;
typedef unsigned int SOCKET;

#define INVALID_SOCKET (SOCKET)(~0)
#define SOCKET_ERROR -1

// Macros for datagram access
#define SET_DWORD( p, o, v ) (*( UINT32 * )( ( (UINT8 *)(p) ) + o ) = htonl((v)))
#define GET_DWORD( p, o ) (ntohl( *( UINT32 * )( ( (UINT8 *)(p) ) + o ) ))
#define SET_WORD( p, o, v ) (*( UINT16 * )( ( (UINT8 *)(p) ) + o ) = htons((v)))
#define GET_WORD( p, o ) (ntohs( *( UINT16 * )( ( (UINT8 *)(p) ) + o ) ))
#define SET_BYTE( p, o, v ) (*( UINT8 * )( ( (UINT8 *)(p) ) + o ) = (v))
#define GET_BYTE( p, o ) 	( *( UINT8 * )( ( (UINT8 *)(p) ) + o ) )


//===================================================
// GVCP Header defs (command and acknowledge)
//===================================================
#define GVCP_KEY		 0x42							//!< GVCP key (hard-coded value)

#define GVCP_KEY_LENGTH sizeof(UINT8)
#define GVCP_FLAG_LENGTH sizeof(UINT8)
#define GVCP_COMMAND_LENGTH sizeof(UINT16)
#define GVCP_LENGTH_LENGTH sizeof(UINT16)
#define GVCP_ID_LENGTH sizeof(UINT16)

// Command header
#define GVCP_KEY_OFFSET 0
#define GVCP_FLAG_OFFSET (  GVCP_KEY_OFFSET + GVCP_KEY_LENGTH ) 
#define GVCP_COMMAND_OFFSET ( GVCP_FLAG_OFFSET + GVCP_FLAG_LENGTH)
#define GVCP_LENGTH_OFFSET ( GVCP_COMMAND_OFFSET + GVCP_COMMAND_LENGTH )  
#define GVCP_ID_OFFSET ( GVCP_LENGTH_OFFSET + GVCP_LENGTH_LENGTH )
#define GVCP_HEADER_SIZE ( GVCP_ID_OFFSET + GVCP_ID_LENGTH ) 

// Acknowledge header (when different from command header)
#define GVCP_STATUS_OFFSET	0
#define GVCP_ACK_STATUS_OFFSET	0

#define GVCP_ACK_STATUS_LENGTH   sizeof(UINT16)
#define GVCP_ACK_ANSWER_LENGTH   sizeof(UINT16)
#define GVCP_ACK_LENGTH_LENGTH   sizeof(UINT16)
#define GVCP_ACK_ACK_ID_LENGTH   sizeof(UINT16)

#define GVCP_ACK_STATUS_OFFSET   0
#define GVCP_ACK_ANSWER_OFFSET   (GVCP_ACK_STATUS_OFFSET + GVCP_ACK_STATUS_LENGTH)
#define GVCP_ACK_LENGTH_OFFSET   (GVCP_ACK_ANSWER_OFFSET + GVCP_ACK_ANSWER_LENGTH)
#define GVCP_ACK_ACK_ID_OFFSET   (GVCP_ACK_LENGTH_OFFSET + GVCP_ACK_LENGTH_LENGTH)

#define GVCP_ACK_HEADER_SIZE     (GVCP_ACK_ACK_ID_OFFSET + GVCP_ACK_ACK_ID_LENGTH)

// ACTION_CMD Payload offsets
#define GVCP_ACTION_CMD	0x100
#define GVCP_ACTION_ACK	0x101

#define GVCP_ACTION_CMD_SIZE				20
#define GVCP_SCHEDULED_ACTION_CMD_SIZE	28
#define GVCP_ACTION_CMD_DEVICE_KEY_OFFSET ( GVCP_HEADER_SIZE ) 
#define GVCP_ACTION_CMD_GROUP_KEY_OFFSET  ( GVCP_HEADER_SIZE + 4 ) 
#define GVCP_ACTION_CMD_GROUP_MASK_OFFSET ( GVCP_HEADER_SIZE + 8 ) 
#define GVCP_ACTION_CMD_ACTIONTIME_HIGH_OFFSET ( GVCP_HEADER_SIZE + 12 ) 
#define GVCP_ACTION_CMD_ACTIONTIME_LOW_OFFSET  ( GVCP_HEADER_SIZE + 16 ) 

// ACTION_CMD command codes and helper macros.
#define ACTION_CMD_ACK_FLAG             0x01	// ACTION_CMD with ACTION_ACK requested	
#define ACTION_CMD_NOACK_FLAG           0x00	// ACTION_CMD with no ACTION_ACK requested	
#define SCHEDULED_ACTION_CMD_ACK_FLAG   0x81 // Scheduled ACTION_CMD with ACTION_ACK requested		
#define SCHEDULED_ACTION_CMD_NOACK_FLAG 0x80 // Scheduled ACTION_CMD with no ACTION_ACK requested	

#define FILL_GVCP_COMMAND_HEADER( datagram, command_size, command, flag, request_id) \
	{ \
		PUINT8 dg = (PUINT8)(datagram); \
		UINT16 size = (command_size); \
		if ( dg != NULL ) { \
			memset( d, 0, size); \
			SET_BYTE( dg, GVCP_KEY_OFFSET, GVCP_KEY ); \
			SET_BYTE( dg, GVCP_FLAG_OFFSET, (flag) ); \
			SET_WORD( dg, GVCP_COMMAND_OFFSET, (command) ); \
			SET_WORD( dg, GVCP_LENGTH_OFFSET, (UINT16)(size - GVCP_HEADER_SIZE)); \
			SET_WORD( dg, GVCP_ID_OFFSET, (request_id) ); \
		} \
	}
	
#define FILL_ACTION_CMD_MSG( datagram, request_id, device_key, group_key, group_mask ) \
		{ \
			PUINT8 d = (PUINT8)(datagram); \
			if ( d != NULL ) { \
				FILL_GVCP_COMMAND_HEADER( d, GVCP_ACTION_CMD_SIZE, GVCP_ACTION_CMD, ACTION_CMD_ACK_FLAG, request_id); \
				SET_DWORD( d, GVCP_ACTION_CMD_DEVICE_KEY_OFFSET, (device_key) );  \
				SET_DWORD( d, GVCP_ACTION_CMD_GROUP_KEY_OFFSET, (group_key) ); \
				SET_DWORD( d, GVCP_ACTION_CMD_GROUP_MASK_OFFSET, (group_mask) ); \
			} \
		}
		
#define FILL_ACTION_CMD_MSG_NOACK( datagram, request_id, device_key, group_key, group_mask ) \
		{ \
			PUINT8 d = (PUINT8)(datagram); \
			if ( d != NULL ) { \
				FILL_GVCP_COMMAND_HEADER( d, GVCP_ACTION_CMD_SIZE, GVCP_ACTION_CMD, ACTION_CMD_NOACK_FLAG, request_id); \
				SET_DWORD( d, GVCP_ACTION_CMD_DEVICE_KEY_OFFSET, (device_key) );  \
				SET_DWORD( d, GVCP_ACTION_CMD_GROUP_KEY_OFFSET, (group_key) ); \
				SET_DWORD( d, GVCP_ACTION_CMD_GROUP_MASK_OFFSET, (group_mask) ); \
			} \
		}

	
#define FILL_SCHEDULED_ACTION_CMD_MSG( datagram, request_id, device_key, group_key, group_mask, action_time ) \
		{ \
			PUINT8 d = (PUINT8)(datagram); \
			uint64_t t = (action_time); \
			if ( d != NULL ) { \
				FILL_GVCP_COMMAND_HEADER( d, GVCP_SCHEDULED_ACTION_CMD_SIZE, GVCP_ACTION_CMD, SCHEDULED_ACTION_CMD_ACK_FLAG, request_id); \
				SET_DWORD( d, GVCP_ACTION_CMD_DEVICE_KEY_OFFSET, (device_key) );  \
				SET_DWORD( d, GVCP_ACTION_CMD_GROUP_KEY_OFFSET, (group_key) ); \
				SET_DWORD( d, GVCP_ACTION_CMD_GROUP_MASK_OFFSET, (group_mask) ); \
				SET_DWORD( d, GVCP_ACTION_CMD_ACTIONTIME_HIGH_OFFSET, ((t & 0xFFFFFFFF00000000ULL) >> 32) ); \
				SET_DWORD( d, GVCP_ACTION_CMD_ACTIONTIME_LOW_OFFSET, (t & 0x00000000FFFFFFFFULL)  ); \
			} \
		}
		
#define FILL_SCHEDULED_ACTION_CMD_MSG_NOACK( datagram, request_id, device_key, group_key, group_mask, action_time ) \
		{ \
			PUINT8 d = (PUINT8)(datagram); \
			uint64_t t = (action_time); \
			if ( d != NULL ) { \
				FILL_GVCP_COMMAND_HEADER( d, GVCP_SCHEDULED_ACTION_CMD_SIZE, GVCP_ACTION_CMD, SCHEDULED_ACTION_CMD_NOACK_FLAG, request_id); \
				SET_DWORD( d, GVCP_ACTION_CMD_DEVICE_KEY_OFFSET, (device_key) );  \
				SET_DWORD( d, GVCP_ACTION_CMD_GROUP_KEY_OFFSET, (group_key) ); \
				SET_DWORD( d, GVCP_ACTION_CMD_GROUP_MASK_OFFSET, (group_mask) ); \
				SET_DWORD( d, GVCP_ACTION_CMD_ACTIONTIME_HIGH_OFFSET, ((t & 0xFFFFFFFF00000000ULL) >> 32) ); \
				SET_DWORD( d, GVCP_ACTION_CMD_ACTIONTIME_LOW_OFFSET, (t & 0x00000000FFFFFFFFULL)  ); \
			} \
		}

//===================================================
// Declarations
//===================================================
typedef struct _tNIC
{
	char network_name[IFNAMSIZ];
	unsigned int index;
	unsigned int NICIP;
	SOCKET	sock;
} tNIC;

BOOL get_Adapters_Info(int *numDetected, tNIC **NICinfos);
int  selectAdapter( int numAdapters, tNIC *NICinfo);
void free_Adapters_Info(int numAdapters, tNIC *NICinfos);


char input_ActionCmd();
BOOL send_ActionCmd(tNIC *NICinfo, uint32_t cmd, uint32_t group_key, uint32_t device_key);




#endif   // __ACTIONCMD__H__
