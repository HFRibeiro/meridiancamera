Release Notes for GigE-V Framework for Linux 
Version 2.10.0.0157

For information on installation, hardware and software requirements, and 
complete API documentation, refer to the GigE-V Framework for Linux 
Programmer's Manual available with the SDK download on the Teledyne DALSA 
website at: 

https://www.teledynedalsa.com/en/support/downloads-center/software-development-kits/132/

What's New?
-----------
This release provides support for acquiring camera metadata (GigE Vision chunk data), 
along iwth demos showing Bayer conversion to RGB, and image save to disk using TIFF files.
(See history details below).

Supported Hardware Platforms
---------------------------- 
 x86    : Intel/AMD 32-bit and 64-bit CPUs
 ARM7hf : 32-bit ARM7 with hard-float (hardware floating point)
 ARMsf  : 32-bit ARM with soft-float (software emulated floating point)
 aarch64: 64-bit ARM 

System Requirements
-------------------
 - Linux OS support for Gigabit NIC hardware (kernel 2.6.24 and later)
 - Support for PF_PACKET with RX_RING capability recommended for best 
    performance (usually available with the Wireshark application and/or 
    the libpcap package which is widely installed by default).
 - libcap-dev package is required to use Linux "capabilities" when running 
   as "root" is not desired.
 - libx11-dev / libxext-dev packages are required for using the X11 display 
   window in the example programs.
 - libglade2-dev package is required for building and using the 
   GigE Vision Device Status tool (which uses gtk).


V2.00.0.0108
------------
Base release

V2.01.0.0120
------------
Release : Highlights 
	- Optimized TurboDrive support for Intel/AMD (automatically enabled).
	- Basic TurboDrive support for ARM.
	- Support for BiColor pixel format.
	- Zipped XML files are now deflated using Public Domain code from "miniz.c"
	  rather than relying on the target system to have "gunzip" installed.
	- X11 display function in example programs no longer relies on a specific named
	  font being present.
	- X11 display function in example programs now handles images that are much
	  larger than the desktop,

V2.02.0.0132
------------
Release : Highlights 
	- Fixes for delivery of frames in order of their frame id. (GigeVision Block ID).
	- Fixes for TurboDrive decoding error for 8bit Bayer data.
	- Adds SSE3 support for unpacking mono10Packed/mono12Packed.
	- Adds aarch64 and 32-bit armv8.

V2.10.0.0157 
------------
Updated:
	- Updated GEV_BUFFER_OBJECT structure (for planned GigeVision standard update - to come)
	   - "id" field (Frame block id) is now 64 bits.
	   - added "filename" field to support "file" payloads
	   - added "chunk_data" pointer to ease access to metadata (chunk data).

Added/Modified:
	- Add lsgev tool to list GigeVision devices on the command line (no GUI).
	- Changed camera open functionality to automatically set up GenICam XML feature access.
	- Added "manual_xml_handling" control to GEVLIB_CONFIG_OPTION to disable automatic setup
	  of the GenICam XML feature access should an application not need the default handling.
	- Add option to save image as a tiff file in some demos.
	- Added the genicam_chunk_demo to demonstrate access to  the GenApi::ChunkAdapter 
	- Added support for Chunk (metadata) payloads using SFNC Chunk category feature names.
	- Added "frame" functions to replace "image" functions for new code (metadata is external
	  to the image but is part of the frame payload.)
	- Added handling of Bayer "Packed" pixel formats.
	- Added Bayer to RGB conversion to example programs. (Bayer8 to RGB8, Bayer16 to RGB16, 
	  Bayer16 to RGB8 (for display)).
	- Added a "passthru" mode that disables automatic unpacking of packed pixel formats
	  and automatic decoding of TurboDrive compressed data sets for more compact archiving.
	- Changed example programs to handle possibly undisplayable image data from "passthru" mode.
	- Added "archive_data" demos to save and restore frame sequences to disk in new ".gevbuf" 
	  file format which supports frame containers holding image data (all formats) and 
	  metadata (if enabled). Genie-Nano metadata layout is hardcoded for now until a more 
	  generic version is ready.
	- Added "genie_nano_actioncmd_demo" to demonstrate ACTION_CMD usage.

	New Functions
	   GevInitializeTransfer   - sets up a "frame" transfer to buffers of known size.
	   GevGetPayloadParameters - gets the "payload_size" feature value and the data format.

	Renamed Functions (previous ones still exist / are aliased).
 	      "previous function"      --->   "new function"
	   GevFreeImageTransfer        ---> GevFreeTransfer
	   GevStartImageTransfer       ---> GevStartTransfer
	   GevStopImageTransfer        ---> GevStopTransfer
	   GevAbortImageTransfer       ---> GevAbortTransfer
	   GevWaitForNextImage         ---> GevWaitForNextFrame
	   GetGetNextImage             ---> GevGetNextFrame
	   GevReleaseImage             ---> GevReleaseFrame
	   GevReleaseImageBuffer       ---> GevReleaseFrameBuffer
	   GevQueryImagetransferStatus ---> GevQueryTransferStatus

Fixes :
	- Fixed race condition in transfer Init/Free that could leak resources.
	- Fixed race condition in Stop/Abort processing that could leave feature tree locked.
	- Fixed packet size (MTU) optimization code to remove repeated open/close of stream channel.
	- Fixed packet resend processing.
	- Fixed an issue with sign extension on packet boundaries during unpacking of 
	  Mono10Packed/Mono12Packed pixels.
	- Fixed usage of pthreads / scheduling attributes internally so that SCHED_RR or SCHED_FIFO
	  can be selected by applications programs and actually work as expected.
	- 3rd Pary camera access now works (as long as a Teledyne camera is also present).







