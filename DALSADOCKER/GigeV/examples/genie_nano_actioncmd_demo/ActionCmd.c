/***************************************************************************************************
Copyright (c) 2018, Teledyne DALSA.
All rights reserved.
----------------------------------------------------------------------------------------------------

ActionCmd.c

Description:
Broadcast a GVCP ACTION_CMD message on a specific network.

**************************************************************************************************/


//====================================================================
// INCLUDE FILES
//====================================================================
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <poll.h>

#include <netinet/tcp.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <time.h>

#include <ActionCmd.h>

#ifndef MAX_PATH
#define MAX_PATH FILENAME_MAX
#endif

#define DEBUG_PRINT_PACKETS  0

//====================================================================
// Member variables
//====================================================================

static uint32_t	m_defaultDeviceKey = 0x00000001;  // Default for Genie Nano family
static uint32_t	m_defaultGroupKey  = 0x00000000;  // Default for Genie Nano family

static UINT8 datagram_out[BUFLEN];		// Actual datagram to send -> command
static UINT8 datagram_in[BUFLEN];		// Actual datagram received -> ack

static BOOL _broadcastMessage(UINT8 *datagram, int size, SOCKET *sock, struct sockaddr_in *dest);
static BOOL _sendMessage(UINT8 *datagram, int size, SOCKET *sock, struct sockaddr_in *dest);
static BOOL _receiveMessage(UINT8 *datagram, int maxSize, SOCKET *sock, struct timeval *timeout);
static BOOL _validate_ack(PUINT8 pDatagram, UINT16 answer, UINT16 id);





int GetNumber()
{
	char input[MAX_PATH] = {0};
	char *nptr = NULL;
	int num;

	scanf("%s", input);
	num = (int) strtol(input, &nptr, 10);

	if (nptr == input )
	{
		return -2;
	}
	return num;
}

char GetKey()
{
   char key = getchar();
   while ((key == '\r') || (key == '\n'))
   {
      key = getchar();
   }
   return key;
}


//====================================================================
// MAIN function
//====================================================================
int main(int argc, char **argv)
{
	printf("Console Action Command Example (C version)\n\n");

	char KEYval = 0;
	tNIC *NICinfo = NULL;
	int nbNIC = 0;
	BOOL valid_selection = TRUE;
	uint32_t group_key = m_defaultGroupKey;
	uint32_t device_key = m_defaultDeviceKey;

	// Get a list of all the network adapters present
	get_Adapters_Info(&nbNIC, &NICinfo);
	if ((nbNIC == 0) || (NICinfo == NULL))
	{
		// No networks.
		printf("No Network adapters were available\n");
		return EXIT_SUCCESS;		
	}	

	while (1)
	{
		// Select an adapter to use.
		int nicId = selectAdapter( nbNIC, NICinfo);
		if (nicId == -1)
		{
			break;
		}
		
		while(nicId != -1)
		{
			// Ask the user which command to send (ACTION_1, ACTION_2 or ACTION_1 & ACTION_2)
			char cmd = input_ActionCmd();

			// Break the while loop and ask the user to choose another network
			if (cmd == 'n' || cmd == 'N')
			{
				break;
			}
			else if (cmd != '1' && cmd != '2' && cmd != '3')
			{
				printf("\nInvalid command!\n");
			}
			else
			{
				while (1)
				{
					// Broadcast the selected command on the selected network
					if (valid_selection)
					{
						uint32_t group_mask = (uint32_t) (cmd - '0');
						printf("Send ACTION_CMD %d ...", group_mask); fflush(stdout);
						if ( send_ActionCmd(&NICinfo[nicId], group_mask, group_key, device_key) )
						{
							printf("ACK received!\n");
						}
						else
						{
							printf("NO ACK received!\n");
						}
					}

					// Give the user the choice to RESEND, CHANGE COMMAND, CHANGE NETWORK or QUIT
					printf("\nChoices:\n");
					printf("  Resend the same command:  Press SPACE <ret>\n");
					printf("  Choose another command:   Press ESC <ret> \n");
					printf("  Choose another network:   Press 'n' <ret>\n");
					printf("  Quit Program              Press 'q' <ret>\n");
					KEYval = GetKey();

					// End the process
					if (KEYval == 'q' || KEYval == 'Q')
					{
						return EXIT_SUCCESS;
					}
					// Stay in the while loop and resend the same cmd
					else if ((int)KEYval == 32)	// SPACE
					{
						KEYval = 0;
						valid_selection = TRUE;
					}	
					// Break the while loop and ask the user to choose another cmd
					else if ((int)KEYval == 27)	// ESC
					{
						KEYval = 0;
						valid_selection = TRUE;
						break;
					}
					// Break the inner while loop
					else if (KEYval == 'n' || KEYval == 'N')	// NIC setup
					{
						valid_selection = TRUE;
						break;
					}						
					else
					{
						printf("\nInvalid selection!\nPlease make another selection.\n");
						valid_selection = FALSE;
					}
				}

				// Break the outer while loop and ask the user to choose another network
				if (KEYval == 'n' || KEYval == 'N')
				{
					KEYval = 0;
					break;
				}
			}
		}	
	}
	
	// Cleanup 
	free_Adapters_Info( nbNIC, NICinfo);
	return(0);
}




//====================================================================
// FUNCTIONS (available for re-use)
//====================================================================

static UINT16 _GetNextRequestID()
{
	static UINT16 requestID = 0;
	requestID += 1;
	if (requestID == 0) requestID = 1;
	return requestID;	
}

//! Platform specific call to get the maximum number of network interfaces in the system.
static int _GetMaxNetworkInterfaces( void)
{
	char          ipbuf[1024];
	struct ifconf ipconfig;
	int           s;
	int           numAdapters;

	// Get a socket to talk to the network driver 
	s = socket(AF_INET, SOCK_DGRAM, 0);
	if(s < 0)
	{
		perror("socket");
		return -1;
	}

	// Query the available interfaces/adapters. 
	ipconfig.ifc_len = sizeof(ipbuf);
	ipconfig.ifc_buf = ipbuf;
	if ( ioctl(s, SIOCGIFCONF, &ipconfig) < 0 )
	{
		return -1;
	}

	// Check the number of adapters against the one requested.
	numAdapters = ipconfig.ifc_len / sizeof(struct ifreq);
	return numAdapters;
}

//! Platform specific call to retrieve MAC address of device
/*!
	This function is used to obtain the MAC and IP address of the network adapter
	\param [in] indexAdapter: Search Index of network adapter (0..(N-1))
	\param [in] nameSize: Size (in bytes) of buffer to receive Interface name. (At least IFNAMSIZ bytes for Linux).
	\patam [out] pIfName: Name of network interface (e.g. "eth0").
	\param [out] pMacHigh: Pointer to upper 16-bit of MAC address
	\param [out] pMacHigh: Pointer to upper 16-bit of MAC address
	\param [out] pMacLow: Pointer to upper 32-bit of MAC address
	\param [out] pIpAddr: Pointer to IP address associated with selected network adapter
	\param [out] pAdapterIndex: Pointer to hold internal adapter index value
	\return TRUE if successful, FALSE otherwise
	\note  This function must be called before calling _SetIpAddress().
*/
BOOL _GetNetworkAdapterInfo (int indexAdapter, int nameSize, char *pIfName, UINT16 *pMacHigh, UINT32 *pMacLow, UINT32 *pIpAddr, UINT32 *pAdapterIndex)
{
	char          ipbuf[1024];
	struct ifconf ipconfig;
	struct ifreq *ipreq;
	int           s;
	int           numAdapters;
	UINT32        ipAddr = 0;

	// Get a socket to talk to the network driver 
	s = socket(AF_INET, SOCK_DGRAM, 0);
	if(s < 0)
	{
		perror("socket");
		return 1;
	}

	// Query the available interfaces/adapters. 
	ipconfig.ifc_len = sizeof(ipbuf);
	ipconfig.ifc_buf = ipbuf;
	if ( ioctl(s, SIOCGIFCONF, &ipconfig) < 0 )
	{
		//GevPrint(GEV_LOG_ERROR, __FILE__, __LINE__, "_GetNetworkAdapterInfo : SIOCGIFCONF failed!\n");
		printf("_GetNetworkAdapterInfo : SIOCGIFCONF failed!\n");
		return FALSE;
	}

	// Check the number of adapters against the one requested.
	numAdapters = ipconfig.ifc_len / sizeof(struct ifreq);
	ipreq       = ipconfig.ifc_req;
	
	if (indexAdapter < numAdapters)
	{
		// Get the request structure for the indicated adapter index
		// (using the internal adapter index - not the order of detection index).
		struct ifreq *item = &ipreq[indexAdapter];
		struct ifreq temp_item;
		temp_item = *item;	

		if ( ioctl(s, SIOCGIFINDEX, &temp_item) < 0 )		
		{
			//GevPrint(GEV_LOG_ERROR, __FILE__, __LINE__, "_GetNetworkAdapterInfo : SIOCGIFINDEX failed!\n");
			printf("_GetNetworkAdapterInfo : SIOCGIFINDEX failed!\n");
			return FALSE;
		}
		else
		{
			if (pAdapterIndex != NULL)
			{ 
				*pAdapterIndex = temp_item.ifr_ifindex;
			}	
		}

		// Get the required information for the particular requested adapter.
		// Get the interface name (always available).
		if ( (nameSize > 0) && (pIfName != NULL) )
		{
			strncpy( pIfName, item->ifr_name, nameSize);
		}
		// Get the IP address
		struct sockaddr_in *sa = (struct sockaddr_in *)&item->ifr_addr;
		ipAddr = ntohl(*((u_int32_t *)&sa->sin_addr));
		if (pIpAddr != NULL)
		{
			*pIpAddr = ipAddr;
		}
	
		// Get the MAC address
		if ( ioctl(s, SIOCGIFHWADDR, item) < 0 )
		{
			//GevPrint(GEV_LOG_ERROR, __FILE__, __LINE__, "_GetNetworkAdapterInfo : SIOCGIFHWADDR failed!\n");
			printf("_GetNetworkAdapterInfo : SIOCGIFHWADDR failed!\n");
			return FALSE;
		}
		else
		{
			struct sockaddr *eth = (struct sockaddr *) &item->ifr_ifru.ifru_hwaddr;
			unsigned long *low = (unsigned long *)&eth->sa_data[2];
			unsigned short *high = (unsigned short*)&eth->sa_data[0];
			printf("Interface %8s : IP %3d.%3d.%3d.%3d : MAC = %02x:%02x:%02x:%02x:%02x:%02x\n", 
				item->ifr_name,
				((ipAddr >> 24)&0xff), ((ipAddr >> 16)&0xff), ((ipAddr >> 8)&0xff), (ipAddr&0xff),
				((ntohs(*high)>> 8)&0x00ff), (ntohs(*high)&0x00ff),
				((ntohl(*low)>> 24)&0x00ff), ((ntohl(*low)>> 16)&0x00ff), ((ntohl(*low)>> 8)&0x00ff), (ntohl(*low)&0x00ff));

			if ((pMacHigh != NULL) && (pMacLow != NULL))
			{
				*pMacHigh = *high;
				*pMacLow = *low;
			}
		}
		return TRUE;
	}

	return FALSE;
}

/*************************************************************
Function get_Adapters_Info

Check for all available networks, show their name and 
IPaddresses to the user. Wait for the user to make a 
choice and save the infos of the chosen network
in *NICinfos.
	
	In : *NICinfos
	Out: BOOL (TRUE = succes, FALSE = error)
************************************************************/
BOOL get_Adapters_Info(int *numDetected, tNIC **NICinfosIn)
{
	if ( (numDetected != NULL) && (NICinfosIn != NULL) )
	{
		int i = 0;
		tNIC *NICinfos = NULL;
		// Get the number of NICs in the system.
		*numDetected = _GetMaxNetworkInterfaces();
		
		// Allocate NIC info structure.
		//*NICinfos = NULL;
		if ( *numDetected > 0 )
		{
			NICinfos = (tNIC *)malloc( *numDetected * sizeof(tNIC) );
		}
		if ( NICinfos == NULL )
		{	
			return FALSE;
		}
		
		// Fill the adapter info data structure.
		for ( i = 0; i < *numDetected; i++ )
		{
			NICinfos[i].sock = INVALID_SOCKET;
			if ( !_GetNetworkAdapterInfo (i, IFNAMSIZ, NICinfos[i].network_name, NULL, NULL, &NICinfos[i].NICIP, &NICinfos[i].index) )
			{
				return FALSE;
			}
		
		}
		*NICinfosIn = NICinfos;
		return TRUE;
	}
	return FALSE;
}

void free_Adapters_Info( int numAdapters, tNIC *NICinfos)
{
	if ( NICinfos != NULL)
	{ 
		int i = 0; 
		for (i = 0; i < numAdapters; i++)
		{
			if (NICinfos[i].sock != INVALID_SOCKET)
			{
				close(NICinfos[i].sock);
			}
		}
		free(NICinfos);
	}
}


int selectAdapter( int numAdapters, tNIC *NICinfos)
{
	if (numAdapters > 0)
	{
		int done = FALSE;
		int selection = -1;
		
		int i;
		printf("\n Detected Network Interfaces : \n");
		printf(" ------------------------------\n");
			
		for (i = 0; i < numAdapters; i++ )
		{
			unsigned int ip = NICinfos[i].NICIP;
			printf("%d - %s : %d.%d.%d.%d\n", i, NICinfos[i].network_name, ((ip >> 24)&0xff), ((ip >> 16)&0xff), ((ip >> 8)&0xff), (ip &0xff) ); 
		
		}
		printf(" ------------------------------\n");
		
		// Get input.
		while( !done)
		{
			printf("\nSelect one of the detected network(s) [0-%d] (-1 to exit) : ", (numAdapters-1));
			fflush(stdout);
			selection = GetNumber();
			if ( (selection >= -1) && (selection < numAdapters) )
			{
				done = TRUE;
			}
		}
		return selection;
	}
	return -1;
}


/*************************************************************
Function input_ActionCmd

Get the user's choice of the number of the action he
wants to send.

	In : NONE
	Out: KEYval
************************************************************/
char input_ActionCmd()
{
	char KEYval;

	printf("\n\nChoices:\n\n");
	printf("  ACTION1:           Press 1\n");
	printf("  ACTION2:           Press 2\n");
	printf("  ACTION1 & ACTION2: Press 3\n");
	printf("\n  Return to network's selection: Press 'n'\n");
	printf("\n>>");

	KEYval = GetKey();

	if (KEYval == 'n' || KEYval == 'N')
		printf("\n");

	else
		printf(" %c\n", (char)KEYval);

	return KEYval;
}


/*************************************************************
Function _broadcastMessage

Initialise broadcast settings on the socket and then call
sendMessage.

	In : *datagram, size, *sock, *dest
	Out: BOOL (TRUE = succes, FALSE = error)
************************************************************/
static BOOL _broadcastMessage(UINT8 *datagram, int size, SOCKET *sock, struct sockaddr_in *dest)
{
	int option;		// used to hold the broadcast option
	struct sockaddr_in destination;

	// Activate broadcast on this socket
	option = 1;
	setsockopt(*sock, SOL_SOCKET, SO_BROADCAST, (char *)&option, sizeof(option));
	destination = *dest;
	destination.sin_addr.s_addr = INADDR_BROADCAST;	// enforce broadcast destination

	// Send the message using the regular function
	if (!_sendMessage(datagram, size, sock, &destination))
		return FALSE;

	// Deactivate broadcast
	option = 0;
	setsockopt(*sock, SOL_SOCKET, SO_BROADCAST, (char *)&option, sizeof(option));

	return TRUE;
}


/*************************************************************
Function _sendMessage

Send the datagram packet through the socket.

	In : *datagram, size, *sock, *dest
	Out: BOOL (TRUE = succes, FALSE = error)
************************************************************/
static BOOL _sendMessage(UINT8 *datagram, int size, SOCKET *sock, struct sockaddr_in *dest)
{
	int nb_bytes;	// number of bytes sent
	int addrLen = sizeof(struct sockaddr_in);

	// Send datagram
	nb_bytes = sendto(*sock, (const char *)datagram, size, 0, (struct sockaddr *)dest, addrLen);
	if (nb_bytes < 0)
	{
		printf("ERROR: Error in sendto (error %d)\n\n\b", errno);
		return FALSE;
	}

	return TRUE;
}


/*************************************************************
Function _receiveMessage

Receive a UDP message through the socket.

In : *datagram, size, *sock, *dest
Out: BOOL (TRUE = succes, FALSE = error)
************************************************************/
static BOOL _receiveMessage(UINT8 *datagram, int maxSize, SOCKET *pSocket, struct timeval *timeout)
{
	fd_set setRead;
	int n = 0;
	int cnt;			// number of instance
	struct sockaddr_in sa;
	socklen_t sa_len = sizeof(sa);

	// Add application socket to list
	FD_ZERO(&setRead);
	FD_SET (*pSocket, &setRead);
	
	if ((int)*pSocket > n)
	{
		n = (int)*pSocket;
	}

	// Perform select operation to block until a port is ready with data
	cnt = select ((n+1), &setRead, NULL, NULL, timeout);
	while ((cnt == SOCKET_ERROR) && (errno == EINTR))
	{
		cnt = select (n, &setRead, NULL, NULL, timeout);
	}

	// Check if at least one port has valid data
	if (cnt > 0)
	{
		// Check if this socket is ready
		if (FD_ISSET(*pSocket, &setRead))
		{
			// Read datagram
			memset(&sa, 0, sizeof(struct sockaddr_in));
			cnt = recvfrom(*pSocket, (char *)datagram, maxSize, 0, (struct sockaddr *)&sa, &sa_len);

#if DEBUG_PRINT_PACKETS
			printf("read %d bytes\n", cnt); 
			if (cnt == 8)
			{
				uint16_t *spt = (uint16_t *)datagram;
				printf(" %04x : %04x \n %04x : %04x \n", (int)ntohs(spt[0]), (int)ntohs(spt[1]), (int)ntohs(spt[2]), (int)ntohs(spt[3]));  
			}
#endif
			return  ( (cnt > 0) ? TRUE : FALSE);
		}
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

/*************************************************************
Function _validate_ack

Validate that the datagram received is really an ACTION_ACK
with a status indicating SUCCESS

	In : *datagram
	Out: BOOL (TRUE = succes, FALSE = error)
************************************************************/
static BOOL _validate_ack(PUINT8 pDatagram, UINT16 answer, UINT16 id)
{
	UINT16 ack_id;
	UINT16 ack_answer;

	// Check if ACK matches the request ID
	if ( pDatagram != NULL)
	{
		ack_id = GET_WORD (pDatagram, GVCP_ID_OFFSET);
		ack_answer = GET_WORD (pDatagram, GVCP_COMMAND_OFFSET);
		if ((ack_id == id) || (ack_answer == answer))
		{
			return TRUE;
		}
	}
	return FALSE;
}

/*************************************************************
Function send_Action_cmd

Send the ACTION_CMD to the chosen network and receive the
ACTION_ACK.

In : NICinfos, cmd, sock_open (TRUE = socket already open)
Out: BOOL (TRUE = succes, FALSE = error)
************************************************************/
BOOL send_ActionCmd(tNIC *NICinfo, uint32_t cmd, uint32_t group_key, uint32_t device_key)
{
	// Open a socket to the NIC with LLA configuration
	// Only if the socket wasn't already open
	if ( NICinfo != NULL )
	{
		struct timeval timeout;
		struct sockaddr_in sock_destination;
		uint16_t request_id;
				
		// See if we already have a socket to access this NIC.
		if ( NICinfo->sock == INVALID_SOCKET )
		{
			struct sockaddr_in sock_source;
			UINT16 source_port = 0;
			int val = 1;

			if ( NICinfo->sock == INVALID_SOCKET)
			{
				//if ((NICinfo->sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == SOCKET_ERROR)
				if ((NICinfo->sock = socket(AF_INET, (SOCK_DGRAM | SOCK_CLOEXEC), 0)) == SOCKET_ERROR)
				{
					printf("socket() failed with error code : %d", errno);
					return FALSE;
				}
			}
			setsockopt( NICinfo->sock, SOL_SOCKET, SO_REUSEADDR, (void *)&val, sizeof(val));

			// Choose a random port between 53248 (0xD000) and 57344 (0xE000) (standard private UDP ports)
			srand(time(NULL));
			source_port = (rand() % 0x1000) + 0xD000;

			sock_source.sin_family = PF_INET;
			sock_source.sin_port = htons((UINT16) source_port);
			sock_source.sin_addr.s_addr = htonl(NICinfo->NICIP);

			// Try to bind the socket
			if (bind(NICinfo->sock, (struct sockaddr *) &sock_source, sizeof(struct sockaddr_in)) == SOCKET_ERROR)
			{
				printf("ERROR: Unable to bind UDP socket (error %d)\n\n\b", errno);
				
				// Cleanup the socket (cannot be used if we can't bind to it).
				close(NICinfo->sock);
				NICinfo->sock = INVALID_SOCKET;
				return FALSE;
			}
		}

		// Re-set the destination for the bound socket.
		sock_destination.sin_family = PF_INET;
		sock_destination.sin_port = htons(GVCP_PORT);

		// Fill the datagram with the standard GVCP header and the payload corresponding to ACTION_CMD
		request_id = _GetNextRequestID();
		//cmd = 0xFFFFFFFF;
		FILL_ACTION_CMD_MSG( datagram_out, request_id, device_key, group_key, cmd ); 

#if DEBUG_PRINT_PACKETS	
		{
			uint16_t *spt = (uint16_t *)datagram_out;
			uint32_t *dpt = (uint32_t *)datagram_out;
			printf(" %04x : %04x \n %04x : %04x \n", (int)ntohs(spt[0]), (int)ntohs(spt[1]), (int)ntohs(spt[2]), (int)ntohs(spt[3]));  
			printf(" %08x \n", (int)ntohl(dpt[2]));  
			printf(" %08x \n", (int)ntohl(dpt[3]));  
			printf(" %08x \n", (int)ntohl(dpt[4]));  
		}
#endif


		// Broadcast the filled datagram through the open socket
		_broadcastMessage(datagram_out, GVCP_ACTION_CMD_SIZE, &NICinfo->sock, &sock_destination);

		// Listen for incoming data (ACTION_ACK)
		//
		// Timeout is the allowed time to wait for an incoming message
		timeout.tv_sec = 0;
		timeout.tv_usec = 500000;

		// Keep listening for data until the timeout has expired 
		// Timeout begins after the last packet received
		// Ex1.: ...ACTION_ACK1 received...ACTION_ACK2 received...500000us...break
		// Ex2.: ...ACTION_ACK1 received...500000us...break
		while (_receiveMessage(datagram_in, BUFLEN, &NICinfo->sock, &timeout));

		return _validate_ack(datagram_in, ACTION_CMD_ACK_FLAG, request_id);
	}
	return FALSE;
}

