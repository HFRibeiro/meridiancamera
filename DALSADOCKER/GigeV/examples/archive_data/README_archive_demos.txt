
This directory contains example programs demonstrating archiving data.


save_data_demo:
	This demo grabs frames from a camera and, when told to, saves them to disk
	in a ".gev_buf" file. A single frame is saved with the "@" command. A sequence
	of frames starts with the "c" command and ends with the "c" command (or when
	a maximum number of frames (FRAME_SEQUENCE_MAX_COUNT) is stored).
	No display is available.
	Metadata (chunk data) can be enabled/disabled.
	Passthru mode can be enabled/disabled.

restore_nano_data_demo:
	This demo restores frame data from a ".gevbuf" file. The code shows how to
	access the data, decompress it (if required), unpack it (if required), and 
	optionally save the data to an image file. Bayer to RGB decoding can be enabled
	at compile time to save Bayer images as color.
	Currently only TIFF image files are supported as output.
	

Note: 
	A "frame" is a container that holds the image data (packed or unpacked) as 
	well as metadata (if enabled).
	A ".gevbuf" file contains a variable number of "frame"s.
	
Please note that an early beta version of TIFF file support functions may
have stored the Blue component first, rather than the Red component as required
by the TIFF specification.



  
