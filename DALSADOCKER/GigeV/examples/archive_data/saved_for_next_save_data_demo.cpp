//============================================================================
//
// Demo showing saving of raw data container to disk as a ".gevbuf" file.
// (Image alone, Image plus Metadata, other(future) ).
// 
// It also stores the XML file used with the (".gevbuf") files for future use.
//

#include "stdio.h"
#include "cordef.h"
#include "GenApi/GenApi.h"		//!< GenApi lib definitions.
#include "gevapi.h"				//!< GEV lib definitions.
#include "gevbuffile.h"

#include <sched.h>

#define MAX_NETIF					8
#define MAX_CAMERAS_PER_NETIF	32
#define MAX_CAMERAS		(MAX_NETIF * MAX_CAMERAS_PER_NETIF)

// Set an upper limit on the number of frames in a sequence 
// (so we don't fill the disk)
#define FRAME_SEQUENCE_MAX_COUNT 1024

// Set upper limit on chunk data size in case of problems with device implementation
// (Adjust this if needed).
#define MAX_CHUNK_BYTES	256

// Enable/disable transfer tuning (buffering, timeouts, thread affinity).
#define TUNE_STREAMING_THREADS 0

#define NUM_BUF	8
void *m_latestBuffer = NULL;

typedef struct tagMY_CONTEXT
{
	GEV_CAMERA_HANDLE camHandle;
	char 					*base_name;
	int 					enable_sequence;
	int 					enable_save;
	BOOL              exit;
}MY_CONTEXT, *PMY_CONTEXT;

// Unique name (seconds resolution in filename)
static void _GetUniqueFilename_sec( char *filename, size_t size, char *basename)
{
	// Create a filename based on the current time (to 1 seconds)
	struct timeval tm;
	uint32_t years, days, hours, seconds;

	if ((filename != NULL) && (basename != NULL) )
	{
		if (size > (16 + sizeof(basename)) )
		{
	
			// Get the time and turn it into a 10 msec resolution counter to use as an index.
			gettimeofday( &tm, NULL);
			years = ((tm.tv_sec / 86400) / 365);
			tm.tv_sec = tm.tv_sec - (years*86400*365);
			days  = (tm.tv_sec / 86400);
			tm.tv_sec = tm.tv_sec - (days * 86400);
			hours = (tm.tv_sec / 3600);
			seconds = tm.tv_sec - (hours * 3600);						
															
			snprintf(filename, size, "%s_%02d%03d%02d%04d", basename, (years-30),days,hours, (int)seconds);
		}
	}
}

// Unique name (10 milliseconds resolution in filename)
static void _GetUniqueFilename_msec( char *filename, size_t size, char *basename)
{
	// Create a filename based on the current time (to 0.01 seconds)
	struct timeval tm;
	uint32_t years, days, hours, seconds;

	if ((filename != NULL) && (basename != NULL) )
	{
		if (size > (16 + sizeof(basename)) )
		{
	
			// Get the time and turn it into a 10 msec resolution counter to use as an index.
			gettimeofday( &tm, NULL);
			years = ((tm.tv_sec / 86400) / 365);
			tm.tv_sec = tm.tv_sec - (years*86400*365);
			days  = (tm.tv_sec / 86400);
			tm.tv_sec = tm.tv_sec - (days * 86400);
			hours = (tm.tv_sec / 3600);
			seconds = tm.tv_sec - (hours * 3600);						
															
			snprintf(filename, size, "%s_%02d%03d%02d%04d%02d", basename, (years-30),days,hours, (int)seconds, (int)(tm.tv_usec/10000));
		}
	}
}


char GetKey()
{
   char key = getchar();
   while ((key == '\r') || (key == '\n'))
   {
      key = getchar();
   }
   return key;
}

void PrintMenu()
{
   printf("GRAB CTL : [S]=stop, [1-9]=snap N, [G]=continuous, [A]=Abort\n");
   printf("SAVE CTL : [@]=Save Next Frame to File\n");
   printf("SAVE CTL : [C]=Toggle Frame Sequence Capture To File (On/Off)\n");
   printf("CONFIG   : [P]=Passthru On/Off, [M]=Metadata On/Off, [T]=TurboMode On/Off\n");
   printf("MISC     : [Q]or[ESC]=end,      [F]=Select Pixel Format\n");
}


void * ImageCaptureThread( void *context)
{
	MY_CONTEXT *captureContext = (MY_CONTEXT *)context;

	if (captureContext != NULL)
	{
		int sequence_init = 0;
		unsigned int sequence_count = 0;
		int sequence_index = 0;
		
		FILE *seqFP = NULL;
		size_t len = 0;
		char filename[FILENAME_MAX] = {0};
		
		if ( captureContext->base_name != NULL)
		{
			len = strlen(captureContext->base_name);
			strncpy( filename, captureContext->base_name, len);
		}
		
		// While we are still running.
		while(!captureContext->exit)
		{
			GEV_BUFFER_OBJECT *img = NULL;
			GEV_STATUS status = 0;
	
			// Wait for images to be received
			status = GevWaitForNextImage(captureContext->camHandle, &img, 1000);

			if ((img != NULL) && (status == GEVLIB_OK))
			{
				if (img->status == 0)
				{
					m_latestBuffer = img->address;
				
					if ((captureContext->enable_sequence) || (sequence_init == 1))
					{
						captureContext->enable_save = FALSE; // Don't do both !!
						if (!sequence_init)
						{
							// Init the capture sequence
							snprintf( &filename[len], (FILENAME_MAX-len-1), "_seq_%06d.gevbuf", sequence_index++);
							printf("%s\n", filename); //?????
							seqFP = GEVBUFFILE_Create( filename );
							if (seqFP != NULL)
							{
								printf("Store sequence to : %s\n", filename);
								sequence_init = 1;
							}
						}
						if (seqFP != NULL)
						{
							if ( GEVBUFFILE_AddFrame( seqFP, img ) > 0)
							{
								printf("Add to Sequence : Frame %llu\r", (unsigned long long)img->id); fflush(stdout);
								sequence_count++;
								if (sequence_count > FRAME_SEQUENCE_MAX_COUNT)
								{
									printf("\n Max Sequence Frame Count exceeded - closing sequence file\n");
									captureContext->enable_sequence = 0;								
								}
							}
							else
							{
								printf("Add to Sequence : Data Not Saved for Frame %llu\r", (unsigned long long)img->id); fflush(stdout);
							}
						}
						// See if we  are done.
						if ( !captureContext->enable_sequence )
						{
							GEVBUFFILE_Close( seqFP, sequence_count );
							printf("Complete sequence : %s has %d frames\n", filename, sequence_count);
							sequence_count = 0;
							sequence_init = 0;							
						}
						
					}
					else if (captureContext->enable_save)
					{
						// Save image (example only).
						// Note : For better performace, some other scheme with multiple 
						//        images (sequence) in a file using file mapping is needed.
						
						snprintf( &filename[len], (FILENAME_MAX-len-1), "_%09llu.gevbuf", (unsigned long long)img->id);
						GEVBUFFILE_SaveSingleBufferObject( filename, img );
						
						// Turn off file save for next frame
						captureContext->enable_save = 0;
						printf("Single frame saved as : %s\n", filename);
						printf("Frame %llu\r", (unsigned long long)img->id); fflush(stdout);
					}
					else
					{
						//printf("chunk_data = %p  : chunk_size = %d\n", img->chunk_data, img->chunk_size); //???????????
						printf("Frame %llu\r", (unsigned long long)img->id); fflush(stdout);
					}
				}
				else
				{
					// Image had an error (incomplete (timeout/overflow/lost)).
					// Do any handling of this condition necessary.
					printf("Frame %llu : Status = %d\n", (unsigned long long)img->id, img->status); 
				}
			}
			
			// Synchonrous buffer cycling (saving to disk takes time).
			if (img != NULL)
			{
				// Release the buffer back to the image transfer process.
				GevReleaseImage( captureContext->camHandle, img);
			}
		}
	}
	pthread_exit(0);	
}


int IsTurboDriveAvailable(GEV_CAMERA_HANDLE handle)
{
	int type;
	UINT32 val = 0;
	
	if ( 0 == GevGetFeatureValue( handle, "transferTurboCurrentlyAbailable",  &type, sizeof(UINT32), &val) )
	{
		// Current / Standard method present - this feature indicates if TurboMode is available.
		// (Yes - it is spelled that odd way on purpose).
		return (val != 0);
	}
	else
	{
		// Legacy mode check - standard feature is not there try it manually.
		char pxlfmt_str[64] = {0};

		// Mandatory feature (always present).
		GevGetFeatureValueAsString( handle, "PixelFormat", &type, sizeof(pxlfmt_str), pxlfmt_str);

		// Set the "turbo" capability selector for this format.
		if ( 0 != GevSetFeatureValueAsString( handle, "transferTurboCapabilitySelector", pxlfmt_str) )
		{
			// Either the capability selector is not present or the pixel format is not part of the 
			// capability set.
			// Either way - TurboMode is NOT AVAILABLE.....
			return 0; 
		}
		else
		{
			// The capabilty set exists so TurboMode is AVAILABLE.
			// It is up to the camera to send TurboMode data if it can - so we let it.
			return 1;
		}
	}
	return 0;
}

int GetNumber()
{
	char input[MAX_PATH] = {0};
	char *nptr = NULL;
	int num;

	scanf("%s", input);
	num = (int) strtol(input, &nptr, 10);

	if (nptr == input )
	{
		return -2;
	}
	return num;
}

int GetPixelFormatSelection(GEV_CAMERA_HANDLE handle, int size, char *pixelFormatString)
{
	int status = -1;
	int count = 0;
	int type = 0;

	// Get the node map.
	GenApi::CNodeMapRef *Camera = static_cast<GenApi::CNodeMapRef*>(GevGetFeatureNodeMap(handle));
	if (Camera)
	{
		try 
		{
			// Access the PixelFormat (mandatory) enumeration
			GenApi::CEnumerationPtr ptrPixFormat = Camera->_GetNode("PixelFormat");
			GenApi::NodeList_t selection; 
			GenApi::StringList_t availableFormats; 
			ptrPixFormat->GetEntries( selection );
			
			// See if there is more than one possible format
			count = (int)selection.size();
			if (count == 1)
			{
				// Only one pixel format - this is it.
				status = GevGetFeatureValueAsString(handle, "PixelFormat", &type, size, pixelFormatString);
				return status;
			}
			else
			{
				// Multiple pixel formats available - Iterate to present them for selection.
				int numFormats = 0;
				int i = 0;
				int num = 0;
				int done = 0;
				for ( i = 0; i < count; i++ )
				{
					GenApi::CEnumEntryPtr entry( selection[i] );
					if ( (GenApi::NI != entry->GetAccessMode()) && ( GenApi::NA != entry->GetAccessMode()))
					{
						numFormats++;
						availableFormats.push_back(entry->GetSymbolic());
					}
				} 
							
				if ( numFormats > 1 )
				{
					while(!done)
					{
						printf("\n Multiple PixelFormats available - select one by index/id :\n");
						for ( i = 0; i < numFormats; i++ )
						{
							printf("    [%02d] = %s \n", i, (const char *)(availableFormats[i]));
						}
						printf("Enter index (0 to %d) :", (numFormats-1));
						fflush(stdout);
						num = GetNumber();
						if ((num >= 0) && (num < numFormats))
						{
							done = 1;
						}
						else
						{
							printf(" \t\t%d : <out of range> ",num);
						}
					}
				}
				else
				{
					num = 0;
					printf("\n Pixel format set to %s \n", (const char *)(availableFormats[i]));					
				} 		
				strncpy(pixelFormatString, (const char *)(availableFormats[num]), size);
				status = 0;
			}
		}
		CATCH_GENAPI_ERROR(status);		
	}
	return status;
}


int main(int argc, char* argv[])
{
	GEV_DEVICE_INTERFACE  pCamera[MAX_CAMERAS] = {0};
	GEV_STATUS status;
	int numCamera = 0;
	int camIndex = 0;
	MY_CONTEXT context = {0};
   pthread_t  tid;
	char c;
	int done = FALSE;
	int turboDriveAvailable = 0;
	char uniqueName[FILENAME_MAX];
	char filename[FILENAME_MAX] = {0};
	uint32_t macLow = 0; // Low 32-bits of the mac address (for file naming).

	// Boost application RT response (not too high since GEV library boosts data receive thread to max allowed)
	// SCHED_FIFO can cause many unintentional side effects.
	// SCHED_RR has fewer side effects.
	// SCHED_OTHER (normal default scheduler) is not too bad afer all.
	if (0)
	{
		//int policy = SCHED_FIFO;
		int policy = SCHED_RR;
		pthread_attr_t attrib;
		int inherit_sched = 0;
		struct sched_param param = {0};

		// Set an average RT priority (increase/decrease to tuner performance).
		param.sched_priority = (sched_get_priority_max(policy) - sched_get_priority_min(policy)) / 2;
		
		// Set scheduler policy
		pthread_setschedparam( pthread_self(), policy, &param); // Don't care if it fails since we can't do anyting about it.
		
		// Make sure all subsequent threads use the same policy.
		pthread_attr_init(&attrib);
		pthread_attr_getinheritsched( &attrib, &inherit_sched);
		if (inherit_sched != PTHREAD_INHERIT_SCHED)
		{
			inherit_sched = PTHREAD_INHERIT_SCHED;
			pthread_attr_setinheritsched(&attrib, inherit_sched);
		}
	}

	//============================================================================
	// Greetings
	printf ("\nGigE Vision Library GenICam C Example Program (save_data) (%s)\n", __DATE__);
	printf ("Copyright (c) 2018, DALSA. (No restrictions on further use)\n\n");

	//===================================================================================
	// Set default options for the library.
	{
		GEVLIB_CONFIG_OPTIONS options = {0};

		GevGetLibraryConfigOptions( &options);
		//options.logLevel = GEV_LOG_LEVEL_OFF;
		//options.logLevel = GEV_LOG_LEVEL_TRACE;
		options.logLevel = GEV_LOG_LEVEL_NORMAL;
		GevSetLibraryConfigOptions( &options);
	}

	//====================================================================================
	// DISCOVER Cameras
	//
	// Get all the IP addresses of attached network cards.

	status = GevGetCameraList( pCamera, MAX_CAMERAS, &numCamera);

	printf ("%d camera(s) on the network\n", numCamera);

	// Select the first camera found (unless the command line has a parameter = the camera index)
	if (numCamera != 0)
	{
		if (argc > 1)
		{
			sscanf(argv[1], "%d", &camIndex);
			if (camIndex >= (int)numCamera)
			{
				printf("Camera index out of range - only %d camera(s) are present\n", numCamera);
				camIndex = -1;
			}
		}

		if (camIndex != -1)
		{
			//====================================================================
			// Connect to Camera
			//
			// Direct instantiation of GenICam XML-based feature node map.
			int i;
			int type;
			UINT32 val = 0;
			UINT32 isLocked = 0;
			UINT32 height = 0;
			UINT32 width = 0;
			UINT32 format = 0;
			UINT32 maxHeight = 1600;
			UINT32 maxWidth = 2048;
			UINT32 maxDepth = 2;
			UINT64 size;
			UINT64 payload_size;
			int numBuffers = NUM_BUF;
			PUINT8 bufAddress[NUM_BUF];
			GEV_CAMERA_HANDLE handle = NULL;

			//====================================================================
			// Open the camera.
			status = GevOpenCamera( &pCamera[camIndex], GevExclusiveMode, &handle);
			if (status == 0)
			{
				//=================================================================
				// GenICam feature access via Camera XML File enabled by "open"
				// 
				// Copy the XML file into the current directory (it may be needed in the "data_restore_demo").
				char xmlFileName[MAX_PATH] = {0};
					
				status = GevGetGenICamXML_FileName( handle, sizeof(xmlFileName), xmlFileName);
				if ( status == GEVLIB_OK)
				{
					int len;
					char command[MAX_PATH] = {0};
					
					len = strlen(xmlFileName);
									
					sprintf( command, "cp \"%s\" . ", xmlFileName );
					system(command);

					i = len;
					while( i > 0)
					{
						i--;
						if (xmlFileName[i] == '/')
							break;
					}
					i++;

					printf("XML stored as %s\n", &xmlFileName[i]);
				}
				status = 0;
			}

			// Get the low part of the MAC address (use it as part of a unique file name for saving images).
			// Generate a unique base name to be used for saving image files
			// based on the last 3 octets of the MAC address.
			macLow = pCamera[camIndex].macLow;
			macLow &= 0x00FFFFFF;
			snprintf(uniqueName, sizeof(uniqueName), "img_%06x", macLow); 
			
			// If there are multiple pixel formats supported on this camera, get one.
			{
				char feature_name[MAX_GEVSTRING_LENGTH] =  {0};
				GetPixelFormatSelection( handle, sizeof(feature_name), feature_name);
				if ( GevSetFeatureValueAsString(handle, "PixelFormat", feature_name) == 0)
				{
					printf("\n\tUsing selected PixelFormat = %s\n\n", feature_name);
				}				
			}
		
			// Go on to adjust some API related settings (for tuning / diagnostics / etc....).
			if ( status == 0 )
			{
				GEV_CAMERA_OPTIONS camOptions = {0};

				// Adjust the camera interface options if desired (see the manual)
				GevGetCameraInterfaceOptions( handle, &camOptions);
				//camOptions.heartbeat_timeout_ms = 60000;		// For debugging (delay camera timeout while in debugger)
				camOptions.heartbeat_timeout_ms = 5000;		// Disconnect detection (5 seconds)
				camOptions.enable_passthru_mode = FALSE;
#if TUNE_STREAMING_THREADS
				// Some tuning can be done here. (see the manual)
				camOptions.streamFrame_timeout_ms = 1001;				// Internal timeout for frame reception.
				camOptions.streamNumFramesBuffered = 4;				// Buffer frames internally.
				camOptions.streamMemoryLimitMax = 64*1024*1024;		// Adjust packet memory buffering limit.	
				camOptions.streamPktSize = 9180;							// Adjust the GVSP packet size.
				camOptions.streamPktDelay = 10;							// Add usecs between packets to pace arrival at NIC.
				
				// Assign specific CPUs to threads (affinity) - if required for better performance.
				{
					int numCpus = _GetNumCpus();
					if (numCpus > 1)
					{
						camOptions.streamThreadAffinity = numCpus-1;
						camOptions.serverThreadAffinity = numCpus-2;
					}
				}
#endif
				// Write the adjusted interface options back.
				GevSetCameraInterfaceOptions( handle, &camOptions);

				//===========================================================
				// Set up the frame information.....
				printf("Camera ROI set for \n");
				GevGetFeatureValue(handle, "Width", &type, sizeof(width), &width);
				printf("\tWidth = %d\n", width);
				GevGetFeatureValue(handle, "Height", &type, sizeof(height), &height);
				printf("\tHeight = %d\n", height);
				GevGetFeatureValue(handle, "PixelFormat", &type, sizeof(format), &format);
				printf("\tPixelFormat  = 0x%x\n", format);
				
				if (camOptions.enable_passthru_mode)
				{
					printf("\n\tPASSTHRU Mode is ON\n");
				}
				
				if (IsTurboDriveAvailable(handle))
				{
					printf("\n\tTurboDrive is : \n");
					val = 1;
					if ( GevGetFeatureValue(handle, "transferTurboMode", &type, sizeof(UINT32), &val) == 0)
					{
						if (val == 1)
						{
							printf("ON\n");
						}
						else
						{
							printf("OFF\n");
						}
					}
				}
				else
				{
					printf("\t*** TurboDrive is NOT Available ***\n");
				}

				printf("\n");
				//
				// End frame info
				//============================================================
				
				if (status == 0)
				{
					//=================================================================
					// Set up a grab/transfer from this camera based on the settings...
					//
					GevGetPayloadParameters( handle,  &payload_size, (UINT32 *)&type);
					maxHeight = height;
					maxWidth = width;
					maxDepth = GetPixelSizeInBytes(format);

					// Calculate the size of the image buffers.
					// (Adjust the number of lines in the buffer to fit the maximum expected 
					//	 chunk size - just in case it gets enabled !!!)
					{
						int extra_lines = (MAX_CHUNK_BYTES + width - 1) / width;
						size = GetPixelSizeInBytes(format) * width * (height + extra_lines);
					}
					
					// Allocate image buffers
					// (Either the image size or the payload_size, whichever is larger - allows for packed pixel formats and metadata).
					size = (payload_size > size) ? payload_size : size;
					for (i = 0; i < numBuffers; i++)
					{
						bufAddress[i] = (PUINT8)malloc(size);
						memset(bufAddress[i], 0, size);
					}
					

					// Generate a file name from the unique base name 
					// (leave at least 16 digits for index and extension)
					_GetUniqueFilename_sec(filename, (sizeof(filename)-17), uniqueName);
					
					// Initialize a transfer with synchronous buffer handling.
					// (To avoid overwriting data buffer while saving to disk).
					status = GevInitializeTransfer( handle, SynchronousNextEmpty, size, numBuffers, bufAddress);

					// Create a thread to receive images from the API and save them
					context.camHandle = handle;
					context.base_name = filename;
					context.exit = FALSE;
		   		pthread_create(&tid, NULL, ImageCaptureThread, &context); 

					
		         // Call the main command loop or the example.
		         PrintMenu();
		         while(!done)
		         {
		            c = GetKey();

		            // Toggle passthru mode
		            if ((c == 'P') || (c == 'p'))
		            {
							isLocked = FALSE;
							GevGetFeatureValue(handle, "TLParamsLocked", &type, sizeof(UINT32), &isLocked);
							if (!isLocked)
							{
								// Free the transfer and re-intitialize it to pick up the passthru mode.
								// (Need to end the thread and restart it).
								GevAbortTransfer(handle);
								context.exit = TRUE;
								pthread_join( tid, NULL);      
								status = GevFreeTransfer(handle);
								
								// Get the camera options.
								GevGetCameraInterfaceOptions( handle, &camOptions);
								if (!camOptions.enable_passthru_mode)
								{
									camOptions.enable_passthru_mode = TRUE;
									printf("\n\t PASSTRHU mode ENABLED\n");
								}
								else
								{
									camOptions.enable_passthru_mode = FALSE;
									printf("\n\t PASSTRHU mode DISABLED\n");
								}
								// Set the toggled option.
								GevSetCameraInterfaceOptions( handle, &camOptions);
								
								// Re-init the transfer.
								status = GevInitializeTransfer( handle, SynchronousNextEmpty, size, numBuffers, bufAddress);

								// Generate an updated file name from the unique base name 
								// and restart the thread
								_GetUniqueFilename_sec(filename, (sizeof(filename)-17), uniqueName);
								context.camHandle = handle;
								context.base_name = filename;
								context.exit = FALSE;
								pthread_create(&tid, NULL, ImageCaptureThread, &context); 
								
							}
							else
							{
								printf("\t *** Transfer Active : Stop/Abort and try again ***\n"); 
							}
						}

		            // Toggle metadata mode
		            if ((c == 'M') || (c == 'm'))
		            {
							isLocked = FALSE;
							GevGetFeatureValue(handle, "TLParamsLocked", &type, sizeof(UINT32), &isLocked);
							if (!isLocked)
							{
								// If metadata is enabled / disable it.
								GevGetFeatureValue(handle, "ChunkModeActive", &type, sizeof(UINT32), &val);
								if ( val == 0)
								{
									// Not active - activate it.
									GevSetFeatureValueAsString(handle, "chunkCompatibilityMode", "GenAPI");
									val = 1;
									GevSetFeatureValue(handle, "ChunkModeActive", sizeof(UINT32), &val);
									printf("\n\t Chunk Mode (Metadata) is ENABLED\n");
								}
								else
								{
									val = 0;
									GevSetFeatureValue(handle, "ChunkModeActive", sizeof(UINT32), &val);
									printf("\n\t Chunk Mode (Metadata) is OFF\n");
								}
							}
							else
							{
								printf("\t *** Transfer Active : Stop/Abort and try again ***\n"); 
							}
						}
		            // Toggle turboMode
		            if ((c == 'T') || (c=='t'))
		            {
							// See if TurboDrive is available.
							turboDriveAvailable = IsTurboDriveAvailable(handle);
							if (turboDriveAvailable)
							{
								val = 1;
								GevGetFeatureValue(handle, "transferTurboMode", &type, sizeof(UINT32), &val);
								val = (val == 0) ? 1 : 0;
								GevSetFeatureValue(handle, "transferTurboMode", sizeof(UINT32), &val);
								GevGetFeatureValue(handle, "transferTurboMode", &type, sizeof(UINT32), &val);
								if (val == 1)
								{
									printf("\n\tTurboMode Enabled\n"); 	
								}
								else
								{
									printf("\n\tTurboMode Disabled\n"); 	
								}														
							}
							else
							{
								printf("\n*** TurboDrive is NOT Available for this device/pixel format combination ***\n");
							}
		            }
		            
		            // Stop
		            if ((c == 'S') || (c=='s') || (c == '0'))
		            {
							GevStopTransfer(handle);
		            }
		            //Abort
		            if ((c == 'A') || (c=='a'))
		            {
	 						GevAbortTransfer(handle);
						}
		            // Snap N (1 to 9 frames)
		            if ((c >= '1')&&(c<='9'))
		            {
							for (i = 0; i < numBuffers; i++)
							{
								memset(bufAddress[i], 0, size);
							}

							status = GevStartTransfer( handle, (UINT32)(c-'0'));
							if (status != 0) printf("Error starting grab - 0x%x  or %d\n", status, status); 
						}
		            // Continuous grab.
		            if ((c == 'G') || (c=='g'))
		            {
							for (i = 0; i < numBuffers; i++)
							{
								memset(bufAddress[i], 0, size);
							}
	 						status = GevStartTransfer( handle, -1);
							if (status != 0) printf("Error starting grab - 0x%x  or %d\n", status, status); 
		            }
					
		            // Save image
		            if ((c == '@'))
		            {
							context.enable_save = 1;
		            }
		            // Save Sequence (toggle)
		            if ((c == 'C') || (c == 'c'))
		            {
							if (context.enable_sequence == 0)
							{
								context.enable_sequence = 1;
							}
							else
							{
								context.enable_sequence = 0;
							}
		            }

		            if (c == '?')
		            {
		               PrintMenu();
		            }

		            if ((c == 0x1b) || (c == 'q') || (c == 'Q'))
		            {
							GevStopTransfer(handle);
		               done = TRUE;
							context.exit = TRUE;
		   				pthread_join( tid, NULL);      
		            }
		         }

					GevAbortTransfer(handle);
					status = GevFreeTransfer(handle);

					for (i = 0; i < numBuffers; i++)
					{	
						free(bufAddress[i]);
					}
				}
				GevCloseCamera(&handle);
			}
			else
			{
				printf("Error : 0x%0x : opening camera\n", status);
			}
		}
	}

	// Close down the API.
	GevApiUninitialize();

	// Close socket API
	_CloseSocketAPI ();	// must close API even on error


	//printf("Hit any key to exit\n");
	//kbhit();

	return 0;
}

