/* =============================================================

GigE-V Framework for Linux : List discovered GigeVision cameras.

===============================================================*/
#include "stdio.h"
#include "cordef.h"
#include "gevapi.h"				//!< GEV lib definitions.

#define MAX_CAMERAS  128


GEV_STATUS ListAllCameras( int level );

static void usage()
{
		printf("lsgev [options] : List GigeVision devices\n");
		printf(" 'lsgev'      lists : <MAC>@[<CamIP>] on <netname>=[<NICIP>]\n");
		printf(" 'lsgev -v'   lists : <MAC>@[<CamIP>] on <netname>=[<NICIP>] is <Manuf>:<Sn>\n");
		printf(" 'lsgev -vv'  lists : <MAC>@[<CamIP>] on <netname>=[<NICIP>] is <Manuf>:<Model>:<Sn>:<Version>\n");
		printf(" 'lsgev -vvv' lists : <MAC>@[<CamIP>] on <netname>=[<NICIP>] is <Manuf>:<Model>:<Sn>:<Version> aka <UserName>\n");
}


int main(int argc, char* argv[])
{
	int level = 0;

	if ((argc > 2))
	{
		usage();
		exit(-1);
	}

	// Get the optional parameter.
	if (argc > 1)
	{
		if ( 0 == strncmp(argv[1], "-vvv", 4) )
		{
			level = 3;
		}
		else if ( 0 == strncmp(argv[1], "-vv", 3) )
		{
			level = 2;
		}
		else if ( 0 == strncmp(argv[1], "-v", 2) )
		{
			level = 1;
		}
		else 
		{
			usage();
			exit(0);
		}
	}

	ListAllCameras( level );

}
// This function lists all cameras detected and prints their information to stdout.
// (Format is " <Manuf>:<Model>:<Sn>:<Version>:<DeviceUserName> aka [MAC]@[<CameraIP>] on <netname> aka [<NIC IP>] "
GEV_STATUS ListAllCameras( int level )
{
	GEV_STATUS status = -1;
	GEV_DEVICE_INTERFACE *pCamera = NULL;
	int numCamera = 0;
	int i;

	// Initialize the API.
	GevApiInitialize();
		
	// Allocate storage for the camera data returned from the API.
	pCamera = (GEV_DEVICE_INTERFACE *) malloc(MAX_CAMERAS * sizeof(GEV_DEVICE_INTERFACE));
	if (pCamera != NULL)
	{
		struct ifreq item = {0};
		int s;
		s = socket(AF_INET, SOCK_DGRAM, 0);
		if(s < 0)
		{
			perror("socket");
			return -1;
		}
		
		memset( pCamera, 0, (MAX_CAMERAS * sizeof(GEV_DEVICE_INTERFACE)));
		status = GevGetCameraList( pCamera, MAX_CAMERAS, &numCamera);

		if (numCamera != 0)
		{
			if (numCamera > MAX_CAMERAS)
			{
				numCamera = MAX_CAMERAS;
			}

			// Cameras found - update list display for "numCamera" entries.		
			for (i = 0; i < numCamera; i++)
			{
				// Get the network name for the NIC
				char netname[IFNAMSIZ+1] = {0};
					
				{
					item.ifr_ifindex = pCamera[i].host.ifIndex;
					if ( ioctl(s, SIOCGIFNAME, &item) < 0 )
					{
						strncpy(netname, "???", IFNAMSIZ);
					}
					else
					{
						strncpy(netname, item.ifr_name, IFNAMSIZ);
					}
					
				}


				// Level 1 output.
				printf("[%02X:%02X:%02X:%02X:%02X:%02X]@[%d.%d.%d.%d] on %s=[%d.%d.%d.%d] ", \
				(pCamera[i].macHigh & 0x00ff00)>>8,   (pCamera[i].macHigh & 0x00ff), \
				(pCamera[i].macLow & 0xff000000)>>24, (pCamera[i].macLow & 0x00ff0000)>>16, \
				(pCamera[i].macLow & 0x0000ff00)>>8,  (pCamera[i].macLow & 0x000000ff), \
				(pCamera[i].ipAddr & 0xff000000)>>24, (pCamera[i].ipAddr & 0x00ff0000)>>16, \
				(pCamera[i].ipAddr & 0x0000ff00)>>8,  (pCamera[i].ipAddr & 0x000000ff), \
				netname, \
				(pCamera[i].host.ipAddr & 0xff000000)>>24, (pCamera[i].host.ipAddr & 0x00ff0000)>>16, \
				(pCamera[i].host.ipAddr & 0x0000ff00)>>8,  (pCamera[i].host.ipAddr & 0x000000ff)); 						
				
				if (level == 1 )
				{
					printf("is [%s:%s]", pCamera[i].manufacturer, pCamera[i].serial);
					
				}
				if (level > 1 )
				{
					printf("is [%s:%s:%s:ver%s]", pCamera[i].manufacturer, pCamera[i].model, pCamera[i].serial, pCamera[i].version);
					
				}
				if (level > 2)
				{
					printf(" aka %s", pCamera[i].username);
				}
				printf("\n");

			}
		}
		else
		{
			printf("0 cameras detected\n");
		}
		close(s);
	}
	free(pCamera);
	return status;						
}
